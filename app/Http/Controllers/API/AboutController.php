<?php

namespace App\Http\Controllers\API;


use App\Models\Notification;

use App\Http\Controllers\Controller;
use App\Models\About;


use App\Http\Resources\About as AboutResource;
use App\Http\Resources\Notification as NotificationResource;
use App\Http\Resources\NotificationCollection as NotificationCollection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

class AboutController extends Controller
{

    public $successStatus = 200;


    public function about()
    {
      $about = About::find(1);
      return response()->json(new AboutResource($about));
    }

    public function notification()
    {
        $notifications = Notification::where('notifiable_id',Auth::id())->get();
        foreach ($notifications as $notification){
            $notification->data = json_decode($notification->data);
            $notification['donation_date'] = $notification->created_at->formatLocalized('%d %b');
        }
        return response()->json(new NotificationCollection($notifications) , $this->successStatus);
//        return response()->json($notifications , $this->successStatus);
    }

}
