<?php

namespace App\Http\Controllers\API;
use App\Http\Resources\CategoryCollection;
use App\Models\Category;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{

    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::get();
        return response()->json(new CategoryCollection($categories), $this->successStatus);

    }


}
