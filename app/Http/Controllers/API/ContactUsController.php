<?php

namespace App\Http\Controllers\API;

//use App\Http\Resources\Auth;
use App\Models\ContactUs;
use App\Notifications\ContactsRequestNotifications;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

class ContactUsController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' =>'required|min:3|max:191',
            'email' =>'required|email|min:3|max:191',
            'contact_type' =>'required|min:3|max:191',
            'message' =>'required|min:3'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $ContactUs = new ContactUs() ;
        $ContactUs->name = $request->name;
        $ContactUs->email =$request->email;
        $ContactUs->contact_type =$request->contact_type;
        if(Auth::check()){
            $ContactUs->user_id =Auth::user()->id ;
        }
        $ContactUs->message =  $request->message;
        $ContactUs->save();


        $admins = User::where('user_type',2)->get();
        Notification::send($admins, new ContactsRequestNotifications($ContactUs));

        return response()->json(['success'=> 'تم ارسال الرسالة بنجاح'],200);
    }
}
