<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Charity;
use App\Models\Fund;
use App\Notifications\ApiNotifications;
use App\Notifications\UserNotifications;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class DonateController extends Controller
{

    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function donate_records()
    {
        $funds = Fund::where('user_id', Auth::id())->get();
        foreach ($funds as $fund){
            $fund->charity->name;
        }
        return response()->json($funds, $this->successStatus);
    }

    public function charity_show()
    {
        $charity = Charity::get();
        return response()->json($charity, $this->successStatus);
    }

    public function charity_search(Request $request)
    {
        $charity = Charity::where('name', 'LIKE', '%' . $request->name . '%')->get();
        if (count($charity) == 0){
            return response()->json(['Result' => 'لا يوجد نتائج'], 400);
        }
        return response()->json($charity, $this->successStatus);
    }

    public function charity_donate(Request $request)
    {
        $validator = validator()->make($request->all(), [
            'charity_id' => 'required|exists:charities,id',
            'to_be_paid' => 'required|numeric|min:0'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find(Auth::id());
        $admin = User::where('user_type',2)->get();
        if ($user->app_cash < $request->to_be_paid) {
            return response()->json(['error' => 'برجاء شحن المحفظه  '], 400);
        }
        if ($request->to_be_paid == 0) {
            return response()->json(['error' => 'برجاء ادخال المبلغ الصحيح'], 400);
        } else {
            $res = $user->app_cash - $request->to_be_paid;
            $user->update([
                'app_cash' => $res
            ]);

            $fund = Fund::create([
                'user_id' => $user->id,
                'charity_id' => $request->charity_id,
                'to_be_paid' => $request->to_be_paid,
            ]);
            Notification::send($admin, new UserNotifications($fund));
            return response()->json(null, 204);

        }
    }
}
