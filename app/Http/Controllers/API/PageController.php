<?php

namespace App\Http\Controllers\API;

use App\Http\Resources\PageResource;
use App\Models\Page;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public $successStatus = 200;

    public function terms()
    {
        $terms = Page::find(1);

        return response()->json(new PageResource($terms),$this->successStatus);
    }

    public function about_app()
    {
        $about = Page::find(2);
//        $data2 = strip_tags($about->content); // Remove Html
//        $data2 = str_replace("&nbsp;"," ",$data2); // Remove /n//r
//        $data2 = preg_replace("/\r|\n/","",$data2); // Remove /n//r
        return response()->json(new PageResource($about), $this->successStatus);
    }
}
