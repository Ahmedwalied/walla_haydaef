<?php

namespace App\Http\Controllers\API;


use App\Models\Category;
use App\Notifications\AdminNotifications;
use App\Notifications\RegisterNotifications;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\PasswordResetCode;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;


use App\Http\Resources\User as UserResource;
use App\Http\Resources\Auth as AuthResource;


class UserController extends Controller
{


    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|exists:users,phone',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        if (Auth::attempt(['phone' => request('phone'), 'password' => request('password')])) {
            $user = Auth::user();
            if ($user->is_active == 1){
                return response()->json(new AuthResource($user), $this->successStatus);
            }else {
                return response()->json(['error' => 'هذا الهاتف غير مفعل'], 400);
            }
        }
        return response()->json(['error' => 'بينات خاطئه'], 400);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required|unique:users,phone',
            'password' => 'required',
            'confirm_password' => 'required|same:password',

        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $admins = User::where('user_type', 2)->get();
        Notification::send($admins, new AdminNotifications($user));

        return response()->json(null, 204);
    }




    public function update_profile(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'sometimes',
            'phone' => 'sometimes|string|max:255',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }
        $user = User::find(Auth::id());
        $user->update($request->all());
        return response()->json(null,  204);
    }


    public function update_image(Request $request)
    {
        $user = User::find(Auth::id());
        $validator = Validator::make($request->all(), [
            'image' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }



        if($request->hasfile('image')){
            $user_image = $request->image;
            $user_image_new_name =  'images/users/'.rand(0,9999).time().$user_image->getClientOriginalName();
            $user_image->move('images/users',$user_image_new_name);
            $user->image=$user_image_new_name;
        }
        $user->save();

        return response()->json([
            'image' => $user->image
        ],  200);
    }

    public function update_bio(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'bio' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 400);
        }

        $user = User::find(Auth::id());
        $user->bio = $request->bio ;
        $user->save();

        return response()->json(null,  204);
    }

//    public function change_password(Request $request)
//    {
//
//        $validator = Validator::make($request->all(), [
//            'password' => 'required',
//            'new_password' => 'required',
//            'confirm_password' => 'required|same:new_password',
//        ]);
//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()->first()], 400);
//        }
//
//        $user = User::find(Auth::id());
//
//        if (password_verify($request->password, $user->password)) {
//            $user->password = bcrypt($request->new_password);
//            $user->save();
//            return response()->json(null,  204);
//        }
//        else{
//            return response()->json(['error' => 'old password is not correct'], 400);
//        }
//
//    }

    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $user = Auth::user();

        return response()->json(new UserResource($user), $this->successStatus);
    }


    public function user_profile()
    {
        $user = User::find(Auth::id());

        return response()->json(new UserResource($user), $this->successStatus);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */



//    public function password_email(Request $request)
//    {
//
//        $validator = Validator::make($request->all(), [
//            'email' => 'required|email|exists:users,email',
//        ]);
//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()->first()], 400);
//        }
//
//
//        $code = rand(1000,9999);
//        $content = 'Your Code for influstar is :' . $code;
//        $email = $request->email;
//        $smtp_user = 'admin@influstar.com';
//        $site = 'influstar ';
//
//
//        Mail::raw($content, function ($message) use ($email, $smtp_user, $site) {
//            $message->from($smtp_user, $site);
//            $message->to($email);
//            $message->subject($site);
//        });
//        var_dump(Mail:: failures());
//
//        $password_reset_code = new PasswordResetCode;
//        $password_reset_code->email = $email;
//        $password_reset_code->code = $code;
//        $password_reset_code->save();
//
//        return response()->json(null,  204);
//    }

//    public function password_email_code(Request $request)
//    {
//
//        $validator = Validator::make($request->all(), [
//            'code' => 'required||exists:password_reset_codes,code',
//            'email' => 'required|email|exists:users,email|exists:password_reset_codes,email',
//        ]);
//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()->first()], 400);
//        }
//
//        $password_reset_code = PasswordResetCode::where('code', $request->code)->where('email',$request->email)->first();
//        if ($password_reset_code) {
//            return response()->json(null,  204);
//        } else {
//            return response()->json(['error' => 'code is not correct'], '400');
//        }
//
//    }

//    public function password_reset(Request $request)
//    {
//
//        $validator = Validator::make($request->all(), [
//            'code' => 'required',
//            'new_password' => 'required',
//            'confirm_password' => 'required|same:new_password',
//        ]);
//        if ($validator->fails()) {
//            return response()->json(['error' => $validator->errors()->first()], 400);
//        }
//
//        $code = $request->code;
//        $password_reset_code = PasswordResetCode::where('code', $code)->first();
//        if ($password_reset_code) {
//            $user = User::where('email', $password_reset_code->email)->first();
//            $user->password = bcrypt($request->get('new_password'));
//            $user->save();
//            PasswordResetCode::destroy($password_reset_code->id);
//            return response()->json(null,  204);
//        } else {
//            return response()->json(['error' => 'code is not correct'], '400');
//        }
//
//    }

    public function get_wallet()
    {
        $user = User::find(Auth::id());
        if ($user->is_active == 0)
            return 'هذا العميل غير مفعل';
        if ($user->is_active == 1)
        return response()->json([
            'app_cash' => $user->app_cash,
        ] , $this->successStatus);
    }

    public function update_wallet(Request $request)
    {
        $validator = validator()->make($request->all(),[
            'app_cash' => 'required|numeric|min:0',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => 'برجاء ادخال المبلغ الصحيح'], 400);
        }
        $user = User::find(Auth::id());
        $update_cash = $user->app_cash + $request->app_cash;
        $user->update([
            'app_cash' => $update_cash
        ]);
        return response()->json(null, 204);
    }


}
