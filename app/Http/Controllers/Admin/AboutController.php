<?php

namespace App\Http\Controllers\Admin;

use App\Models\About;
use App\Models\About_translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;


class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            $pages = About::all();
            return view('admin.about.all', compact('pages'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            return view('admin.about.single');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image',
        ]);

        foreach (Config::get('languages') as $lang=>$value ){
            $this->validate($request, [
                $lang.'_title' => 'required',
                $lang.'_description' => 'required',
            ]);
        }


        if (file_exists($request->image)) {
            $file = $request->file('image');
            $image = $file->move('images/about', unique_file($file->getClientOriginalName()))
                ->getFilename();
        }
        else
        {
            $image=null;
        }

        $about=new About();
        $about->image = $image;
        $about->save();

        foreach (Config::get('languages') as $lang=>$value ){

            $about_translate = new About_translation();
            $about_translate->about_id = $about->id;
            $about_translate->language = $lang;
            $about_translate->title = $request->get($lang.'_title');
            $about_translate->description = $request->get($lang.'_description');
            $about_translate->save();
        }



        return back()->with('success', trans('message.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $about=About::find($id);
        return view('about',compact('about'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about=About::find($id);
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            return view('admin.about.single', compact('about'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = About::find($id);

        $this->validate($request, [
            'image' => 'image',
        ]);

        foreach (Config::get('languages') as $lang=>$value ){
            $this->validate($request, [
                $lang.'_title' => 'required',
                $lang.'_description' => 'required',
            ]);
        }

        if (file_exists($request->image)) {
            $file = $request->file('image');
            $image = $file->move('images/about/', $file->getClientOriginalName())
                ->getFilename();
        }
        else
        {
            $image=$about->image;
        }


        $about->image = $image;
        $about->save();

        foreach (Config::get('languages') as $lang=>$value ){
            $about_translate = $about->translation($lang)->first();
            $about_translate->title = $request->get($lang.'_title');
            $about_translate->description = $request->get($lang.'_description');
            $about_translate->save();
        }


        return back()->with('success',trans('message.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

