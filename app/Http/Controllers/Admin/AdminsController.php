<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class AdminsController extends Controller
{

    public function index()
    {
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            $users = User::where('user_type','2')->get();
            return view('admin.admins.all', compact('users'));
        }
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('admin.admins.single');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);


        $user = new User();
        $user->name =  $request->get('name');
        $user->user_name =  preg_replace('~[^\pL\d]+~u', '-', $request->get('name'));
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->role = 'admin';
        $user->save();

        return back()->with('success', trans('message.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.admins.single',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'sometimes|string|max:255',
            'email' => 'sometimes|string|email|max:255',
            'password' => 'sometimes|confirmed',
        ]);


        $user = User::find($id);
        $user->name =  $request->get('name');
        $user->user_name =  preg_replace('~[^\pL\d]+~u', '-', $request->get('name'));
        $user->email = $request->get('email');
        $user->password = bcrypt($request->get('password'));
        $user->save();

        return back()->with('success', trans('message.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        return response()->json(['success' =>  trans('message.delete_success')]);
    }
}
