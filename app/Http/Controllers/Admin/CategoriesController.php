<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Category_translation;
use App\Models\City_translation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=Category::get();
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            return view('admin.categories.all', compact('categories'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.single');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image',
        ]);

        foreach (Config::get('languages') as $lang=>$value ){
            $this->validate($request, [
                $lang.'_name' => 'required',
            ]);
        }


        if (file_exists($request->image)) {
            $file = $request->file('image');
            $image = $file->move('images/categories', unique_file($file->getClientOriginalName()))
                ->getFilename();
        }
        else
        {
            $image=null;
        }
        $cat=new Category();
        $cat->image=$image;
        $cat->save();

        foreach (Config::get('languages') as $lang=>$value ){

            $cat_translate = new Category_translation();
            $cat_translate->category_id = $cat->id;
            $cat_translate->language = $lang;
            $cat_translate->name = $request->get($lang.'_name');
            $cat_translate->save();
        }

        return back()->with('success', trans('message.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category=Category::find($id);
        return view('categories',compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=Category::find($id);
        return view('admin.categories.single', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'image' => 'image',
        ]);

        foreach (Config::get('languages') as $lang=>$value ){
            $this->validate($request, [
                $lang.'_name' => 'required',
            ]);
        }

        $cat = Category::find($id);

        if (file_exists($request->image)) {
            $file = $request->file('image');
            $image = $file->move('images/categories', unique_file($file->getClientOriginalName()))
                ->getFilename();

            $old_image = 'images/categories/' . $cat->image;
            if (is_file($old_image)) unlink($old_image);
        }
        else{
            $image=$cat->image;
        }

        $cat->image = $image;
        $cat->save();

        foreach (Config::get('languages') as $lang=>$value ){
            $cat_translate = $cat->translation($lang)->first();
            $cat_translate->name = $request->get($lang.'_name');
            $cat_translate->save();
        }


        return back()->with('success',  trans('message.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category=Category::find($id);
        $old_image = 'images/categories/' . $category->image;
        if (is_file($old_image)) unlink($old_image);
        Category::destroy($id);
        return response()->json(['success' =>  trans('message.delete_success')]);
    }
}
