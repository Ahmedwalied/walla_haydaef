<?php

namespace App\Http\Controllers\Admin;

use App\Models\Charity;
use App\Models\Foundation;
use App\Models\Fund;
use App\Models\Transfer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class CharityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $charities=Charity::orderBy('cash','desc')->get();
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            return view('admin.charities.charity', compact('charities'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $foundation = Foundation::get();
        return view('admin.charities.create',compact('foundation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'description' => 'required',
            'foundation_id' => 'required|exists:foundations,id',

        ]);
        $charity = Charity::create($request->all());
        $charity->cash = 0;
        return back()->with('success', trans('message.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $charity =Charity::find($id);

        $foundation = Foundation::find($charity->foundation_id);

        $funds = Fund::where('charity_id',$charity->id)
            ->orderBy('created_at')->get();
        $sum2 = $funds->sum('cash');

        $transfers = Transfer::where('charity_id',$charity->id)
            ->where('foundation_id',$foundation->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $sum = $transfers->sum('cash');
        return view('admin.charities.show',
            compact('charity','funds','transfers','foundation','sum','sum2'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $charity = Charity::find($id);
        $foundation = Foundation::get();
        return view('admin.charities.edit', compact('foundation','charity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id,Request $request)
    {
        $charity = Charity::find($id);
        if (Input::has('suspended')) {
            $charity->status = request('suspended');
            $charity->save();
            return redirect()->back()->with('success', 'تم تعديل التفعيل بنجاح .');
        }else {
            $charity->update($request->all());
        }
        return back()->with('success', trans('message.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
