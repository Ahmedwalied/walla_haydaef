<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use App\Model\Product;
use App\Models\Category;
use App\Models\ContactUs;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $users = User::all()->count();
        $latestUsers = User::where('created_at', '>=', Carbon::yesterday())->count();
        $countUnRead = Auth::user()->unreadNotifications()->count();
        $unReadNotifications = Auth::user()->unreadNotifications()->paginate(10);
        $messages = ContactUs::all()->count();
        $categories = Category::all()->count();
        return view('admin.dashboard',compact(['users','latestUsers','countUnRead','unReadNotifications','messages','categories']));
    }


}
