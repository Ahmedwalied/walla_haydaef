<?php

namespace App\Http\Controllers\Admin;


use App\Models\Branch;
use App\Models\Foundation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class FoundationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $foundations=Foundation::orderBy('status','desc')->get();
        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
            return view('admin.foundations.foundations', compact('foundations'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.foundations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
            'name' => 'required',
            'address' => 'required',
            'about_us' => 'required',
            'manager_name' => 'required',

       ]);
        $foundation = Foundation::create($request->all());
        $foundation->cash = 0;
        return back()->with('success', trans('message.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $foundation =Foundation::find($id);
        return view('admin.foundations.show',compact('foundation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $foundation=Foundation::find($id);
        return view('admin.foundations.edit', compact('foundation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id ,Request $request)
    {
        $foundation = Foundation::find($id);
        if (Input::has('suspended')) {
            $foundation->status = request('suspended');
            $foundation->save();
            return redirect()->back()->with('success', 'تم تعديل التفعيل بنجاح .');
        }else{
        $foundation->update($request->all());
        }
        return back()->with('success', trans('message.update_success'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
