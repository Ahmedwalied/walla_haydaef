<?php

namespace App\Http\Controllers\Admin;

use App\Models\Charity;
use App\Models\Fund;
use App\Notifications\ApiNotifications;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;

class FundController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $this->validate($request, [
            'cash' => 'required|numeric|min:0',
        ],
            [
                'cash.required' => 'برجاء ادخال المبلغ',
            ]
        );

        $user = User::find($request->user);
        $charity = Charity::find($request->charity_id);
        if ($user->cash < $request->cash) {
            return back()->with('error', 'مبلغ خاطئ');
        } else {

            $charityCash = $request->cash + $charity->cash;
            $charity->update([
                'cash' =>  $charityCash
            ]);
            $res = $user->cash - $request->cash;
            $user->update([
                'cash' => $res
            ]);

            $fund = Fund::create([
                'user_id' => $request->user,
                'charity_id' => $request->charity_id,
                'cash' => $request->cash,
            ]);
            Notification::send($user, new ApiNotifications($fund));
            return back()->with('success', 'تم التبرع بنجاح');
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $charity = Charity::find($id);
        $users = User::get();
        return view('admin.charities.donate',compact('charity','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($request->user_id);
        $userCash =  $user->cash - $request->cash;
        $charity = Charity::find($request->charity_id);
        $cash = $request->cash + $charity->cash;
        $fund = Fund::find($request->id);
        $fundRemaining = $fund->to_be_paid - $fund->cash;
        $fundCash = $request->cash + $fund->cash;
        if ($request->cash > $user->cash){
            return back()->with('error','المبلغ الذي ادخلته غير صحيح');
        }elseif ($request->cash > $fund->to_be_paid){
            return back()->with('error','المبلغ الذي ادخلته اكبر من المراد دفعه');
        }elseif ($userCash < 0){
            return back()->with('error',' مبلغ غير صحيح');
        }elseif ($request->cash > $fundRemaining){
            return back()->with('error',' المبلغ الذي ادخلته اكبر من المراد دفعه');
        }else{
            $charity->update([
                'cash' => $cash
            ]);
            $user->update([
                'cash' => $userCash
            ]);
           $fund->update([
               'cash' => $fundCash
           ]);
        }
        Notification::send($user, new ApiNotifications($fund));
       return back()->with('success','تم التعديل');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
