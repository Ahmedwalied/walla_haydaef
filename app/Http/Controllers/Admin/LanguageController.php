<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class LanguageController extends Controller
{
    public function switchLang($lang)
    {


        if (array_key_exists($lang, Config::get('languages'))) {

        	if(session()->has('lang'))
        		{
        			session()->forget('lang');
        		}
        	session()->put('lang',$lang);

        }
        else
        {
            session()->put('lang','ar');
        }
        return Redirect::back();
    }
}
