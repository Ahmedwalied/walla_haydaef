<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Model\PageTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Helpers;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('admin.pages.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.single');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([

            'title'=>  'required|min:3|max:190',
            'content' => 'required|min:3',

        ]);
        $page = new Page();
        $page->is_active = request('is_active') ? 1 : 0;
        $page->title = $request->get('title');
        $page->content = $request->get('content');
        $page->save();
        return redirect('admin/pages/'.$page->id)->with('success', 'تم اضافة صفحة '.$page->title.' بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        return view('admin.pages.single',compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);

        return view('admin.pages.single',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        if(Input::has('suspended')){
            $page->is_active = request('suspended') ;
            $page->save();
            return redirect()->back()->with('success', 'تم تعديل التفعيل الصفحة بنجاح .');
        }else{
            $this->validate($request,[
                'title'=>  'sometimes|min:3|max:190',
                'content' => 'sometimes|min:3',
            ]);
            $page->is_active = request('is_active')=== null ? 0 : 1;
            $page->title = $request->get('title');
            $page->content = $request->get('content');
            $page->save();
            return redirect()->back()->with('success', 'تم تعديل بيانات الصفحة بنجاح .');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page= Page::findOrFail($id);
        if($page->image){
            if(File::exists($page->image)){
                unlink($page->image);
            }};

        $page->delete();
        return redirect()->back()->with('success', ' تم حذف الصفحة بنجاح .');
    }
}
