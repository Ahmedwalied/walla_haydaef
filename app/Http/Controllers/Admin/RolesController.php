<?php

namespace App\Http\Controllers\Admin;

use App\Model\AppTables;
use App\Model\ContactType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesController extends Controller
{
    function __construct() {
//        $this->middleware('SuperAdmin');
        $this->middleware('CanRead:'.(new Role())->getTable())->only(['index', 'show']);
        $this->middleware('CanCreate:'.(new Role())->getTable())->only(['create','store']);
        $this->middleware('CanUpdate:'.(new Role())->getTable())->only(['edit','update']);
        $this->middleware('CanDelete:'.(new Role())->getTable())->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        @dd((new ContactType())->getTable());
        $roles = Role::all();
        return view('admin.roles.index',compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permissions = Permission::all();
        $tables = AppTables::select('id','title','title_ar')->whereIsActive(1)->get();
        $perms = ['create','read','update','delete'];
        return view('admin.roles.single',compact('permissions','tables','perms'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:roles|min:2|max:255',
        ]);
        $permissions = Permission::all();
        $role = Role::create(['name' => $request->name ]);
        foreach ($permissions as $key=>$perm){
            $name=$request[str_replace(' ','-',$perm->name)];
            if($name){
                $perm1 = Permission::find($name);
                $role->givePermissionTo($perm1);;
            }
        }
        return redirect('admin/roles')->with('success', 'تم اضافة دور '.$role->name.' بنجاح ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $permissions = Permission::all();
        $tables = AppTables::select('id','title','title_ar')->whereIsActive(1)->get();
        $perms = ['create','read','update','delete'];
        return view('admin.roles.single',compact('role','permissions','tables','perms'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:2|max:255|unique:roles,name,'.$id,
        ]);
        $permissions = Permission::all();
        $role = Role::findorfail($id);
        $role->update(['name' => $request->name ]);
        foreach ($permissions as $key=>$perm){
            $name=$request[str_replace(' ','-',$perm->name)];
            if(isset($name)){
                $perm1 = Permission::find($name);
                $role->givePermissionTo($perm1);;
            }else{
                $perm1= $perm->id;
                $role->revokePermissionTo($perm1);
            }
        }
        return redirect('admin/roles')->with('success', 'تم تعديل دور '.$role->name.' بنجاح ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::findOrFail($id);
        $role->syncPermissions();
        $role->delete();
        return redirect('admin/roles')->with('success', 'تم حذف دور '.$role->name.' بنجاح ');
    }
}
