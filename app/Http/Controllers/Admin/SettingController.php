<?php

namespace App\Http\Controllers\Admin;


use App\Http\Requests\SettingsValidation;

use App\Models\Setting;
use App\User;
use Illuminate\Http\Request;

class SettingController extends Controller
{
//    function __construct()
//    {
//        $this->middleware('web');
//    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = Setting::find(1);
        return view('admin.settings',compact('settings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingsValidation $request)
    {
        if($request->hasfile('logo')){
            $file = $request->file('logo');
            $filename ='images/logos/'.time().'-'.$file->getClientOriginalName();
            $file->move('images/logos/', $filename);
        }elseif($request->has('old-logo')){
            $filename = $request['old-logo'];
        }else{
            $filename = 'themes/default/assets/img/logo.png';
        }
        $data = [
              "title" => $request['title'],
              "description" => $request['description'],
              "logo" => $filename,
              "copyrights" => $request['copyrights'],
              "footer_payment" => $request['footer_payment'],
              "work_times" => $request['work_times'],
              "currency" => $request['currency'],
              "address" => $request['address'],
              "whatsapp" => $request['whatsapp'],
              "phone" => $request['phone'],
              "email" => $request['email'],
              "map" => $request['map'],
              "facebook" => $request['facebook'],
              "linkedin" => $request['linkedin'],
              "twitter" => $request['twitter'],
              "snapchat" => $request['snapchat'],
              "youtube" => $request['youtube'],
              "instagram" => $request['instagram'],
        ];
        Setting::updateOrCreate(['id' => 1] , $data);
        return redirect('/admin/settings')->with('success','تم تعديل البيانات بنجاح');
    }
}
