<?php

namespace App\Http\Controllers\Admin;

use App\Models\Charity;
use App\Models\Foundation;
use App\Models\Transfer;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cash' => 'required|numeric|min:0',
        ],
            [
                'cash.required' => 'Sorry, Cash Is Required',
            ]
        );
        $foundation = Foundation::find($request->foundation_id);
        $charity = Charity::find($request->charity_id);
        if ($charity->cash < $request->cash){
            return back()->with('error', 'Wrong Cash');
        }else{
            $charityCash = $charity->cash - $request->cash;
            $charity->update([
                'cash' =>  $charityCash
            ]);
            $foundationCash = $foundation->cash + $request->cash;
            $foundation->update([
                'cash' => $foundationCash
            ]);

            Transfer::create([
                'foundation_id' => $request->foundation_id,
                'charity_id' => $request->charity_id,
                'cash' => $request->cash,
            ]);
            return back()->with('success', 'تم التحويل بنجاح');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
