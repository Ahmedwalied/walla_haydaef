<?php

namespace App\Http\Controllers\Admin;

use App\Models\Fund;
use App\Models\SiteInformation;
use App\User;
use App\UserCard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Input;
class UsersController extends Controller
{


    function __construct()
    {
        $this->middleware('CanRead:' . (new User())->getTable())->only(['index', 'show']);
        $this->middleware('CanCreate:' . (new User())->getTable())->only(['create', 'store']);
        $this->middleware('CanUpdate:' . (new User())->getTable())->only(['edit', 'update']);
        $this->middleware('CanDelete:' . (new User())->getTable())->only(['destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('user_type', 0)->get();
        return view('admin.users.index', compact('users'));

//        if (Route::current()->getPrefix() == '/admin' ||Route::current()->getPrefix() == 'admin/') {
//            $users = User::where('role','user')->get();
//            return view('admin.users.all', compact('users'));
//        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();

        return view('admin.users.single', compact('roles'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);


        if (file_exists($request->image)) {
            $file = $request->file('image');
            $image = $file->move('images/users', unique_file($file->getClientOriginalName()))
                ->getFilename();
        }
        else{
            $image=null;
        }

        $user = new User();
        $user->name = $request->get('name');
        $user->user_name = $request->get('user_name');
        $user->email = $request->get('email');
        $user->phone = $request->get('phone');
        $user->password = bcrypt($request->get('password'));
        $user->image = $image;
        $user->bio = $request->get('bio');
        if ($request->app_cash == null){
            $user->app_cash = 0;
        }else{
            $user->app_cash = $request->get('app_cash');
        }
        $user->user_type = request('user_type');

        $user->is_active = request('is_active') ? 1 : 0;
        if ($request->role !== '0') {
            $user->assignRole($request->role);
        }
        $user->save();

//        $info = SiteInformation::find(1);
//        $content = 'Your password for muscli is :'.$request->get('password');
//        $email=$user->email;
//        $smtp_user=$info->email;
//        $site=$info->site_name;
//
//
//        Mail::send('admin.mailingList.send', [ 'content' => $content], function ($message) use ( $email,$smtp_user,$site)
//        {
//            $message->from($smtp_user,$site);
//            $message->to($email);
//            $message->subject($site);
//        });
//        var_dump( Mail:: failures());
        if($request->role !== '0'){
            return redirect('admin/users-roles')->with('success', trans('message.add_success'));

        }
        else{
            return redirect('admin/users')->with('success', trans('message.add_success'));

        }

//        return back()->with('success', trans('message.add_success'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $funds = Fund::where('user_id',$user->id)->orderBy('created_at','desc')->get();
        return view('admin.users.show',compact('user','funds'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();

        return view('admin.users.single', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (Input::has('suspended')) {
            $user->is_active = request('suspended');
            $user->save();
            return redirect()->back()->with('success', 'تم تعديل التفعيل العضو بنجاح .');
        } else {
            $this->validate($request, [
                'name' => 'sometimes|string|max:255',
                'phone' => 'required|unique:users,phone,'.$id,
            ]);


            if (file_exists($request->image)) {
                $file = $request->file('image');
                $image = $file->move('images/users', unique_file($file->getClientOriginalName()))
                    ->getFilename();
            } else {
                $image = $user->image;
            }

            $user->name = $request->get('name');
            $user->user_name = $request->get('user_name');
            $user->email = $request->get('email');
            $user->bio = $request->get('bio');
            $user->app_cash= $request->get('app_cash');
            $user->cash = $request->get('cash');
            $user->phone = $request->get('phone');
            $user->user_type = request('user_type');

            $user->image = $image;
            if ($request->get('password'))
                $user->password = bcrypt($request->get('password'));
            if ($user->hasAnyRole(Role::all())) {
                $user->removeRole($user->getRoleNames()[0]);
            }
            if ($request->role !== '0') {
                $user->assignRole($request->role);
            }
            $user->save();

            if($request->role !== '0'){
                return redirect('admin/users-roles')->with('success', trans('message.update_success'));

            }
            else{
                return redirect('admin/users')->with('success', trans('message.update_success'));

            }

//            return back()->with('success', trans('message.update_success'));

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
     * @return mixed
     */
    public function usersRoles()
    {
        $users = User::where('user_type', 2)->get();
        $roles = Role::with('users')->get(['id', 'name']);
        return view('admin.users.usersroles',compact('roles', 'users'));
    }
}
