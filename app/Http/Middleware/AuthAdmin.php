<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() ) {
            if (Auth::user()->role != "admin") {
                Auth::logout();
                return redirect('/admin/login')->with('error', trans('admin.not_admin'));
            }
        }
        else{
            return redirect('/admin/login')->with('error', trans('admin.not_admin'));
        }
        return $next($request);
    }
}
