<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CanRead
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!  Auth::user()->hasPermissionTo(explode(':',$request->route()->controllerMiddleware()[0],2)[1].' read') ) {
            return redirect('/admin')->with('error' , 'ليس لديك صلاحية الدخول');
        }
        return $next($request);
    }
}
