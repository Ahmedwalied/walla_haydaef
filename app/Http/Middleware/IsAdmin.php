<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()) {
            if(! Auth::user()->is_active==1){
                return redirect('/')->with('error' , 'لم يتم تفعيل حسابك من الإدارة حتى الآن');
            }
            if(! Auth::user()->hasAnyPermission(Permission::pluck('name')->toArray()) ){
                return redirect('/')->with('error' , 'ليس لديك صلاحية الدخول');
            }
            if ( Auth::user()->id ==1 ||  Auth::user()->hasRole('admin')){
                $request->session()->put('SuperAdmin', 1);
            }
            return $next($request);
        }
        return redirect('/admin/login');

    }
}
