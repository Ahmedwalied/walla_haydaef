<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Auth extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => (int) $this->id,
            "name" => (string) $this->name,
            "user_name" => (string) $this->user_name,
            "email" => (string) $this->email,
            "image" => (string) $this->image,
            "bio" => (string) $this->bio,
            "phone" => (string) $this->phone,
            "cash" => (double) $this->cash,
            "app_cash" => (double) $this->app_cash,
            "token" => $this->createToken('MyApp')->accessToken,


        ];
    }
}
