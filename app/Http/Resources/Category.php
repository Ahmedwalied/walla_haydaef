<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Category extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $is_added = \App\Models\Link::where("category_id", $this->id)->where("user_id",auth('api')->id())->first();
        return [
            "id" => (int) $this->id,
            "name" => (string) $this->translation->first()->name ,
            "image" => (string) "/images/categories/".$this->image,
            'is_added'=>(boolean)$is_added,

        ];
    }
}
