<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "type" =>  $this->type ,
            "notifiable" =>  $this->notifiable,
            'data'=> $this->data,
            'donation_date'=> $this->donation_date,
        ];
    }
}
