<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppTables extends Model
{
    protected $fillable = ["title",'title_ar','is_active'];
}
