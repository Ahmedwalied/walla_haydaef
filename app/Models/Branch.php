<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model {

	protected $table = 'branches';
	public $timestamps = true;
	protected $fillable = array('name','foundation_id','address');

	public function foundation()
	{
		return $this->belongsTo('App\Models\Foundation');
	}

}
