<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Charity extends Model {

	protected $table = 'charities';
	public $timestamps = true;
	protected $fillable = array('name', 'address', 'description', 'cash','foundation_id');

	public function foundation()
	{
		return $this->belongsTo('App\Models\Foundation');
	}

	public function notifications()
	{
		return $this->hasMany('App\Models\Notification');
	}

	public function funds()
	{
		return $this->hasMany('App\Models\Fund');
	}

    public function transfers()
    {
        return $this->hasMany('App\Models\Transfer');
    }



}
