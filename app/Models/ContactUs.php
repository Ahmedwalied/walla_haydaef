<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class ContactUs extends Model
{
    use  Notifiable;
    protected $table = 'contactus';
    protected $fillable = ['name','email','user_id','contact_type','message'];
}
