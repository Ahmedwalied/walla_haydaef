<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foundation extends Model {

	protected $table = 'foundations';
	public $timestamps = true;
	protected $fillable = array('name', 'address', 'about_us', 'manager_name','status','cash');

	public function charities()
	{
		return $this->hasMany('App\Models\Charity');
	}

	public function branches()
	{
		return $this->hasMany('App\Models\Branch');
	}

    public function transfers()
    {
        return $this->hasMany('App\Models\Transfer');
    }

}
