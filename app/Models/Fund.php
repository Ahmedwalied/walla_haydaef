<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fund extends Model {

	protected $table = 'funds';
	public $timestamps = true;
	protected $fillable = array('cash','user_id','charity_id','to_be_paid');

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function charity()
	{
		return $this->belongsTo('App\Models\Charity');
	}


}
