<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model {

	protected $table = 'notifications';
	public $timestamps = true;
	protected $fillable = array('type', 'data','read_at','notifiable_type','notifiable_id');


	public function charity()
	{
		return $this->belongsTo('App\Models\Charity');
	}

}
