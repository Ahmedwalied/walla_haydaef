<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable=(['title','content','is_active']);
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('Y-m-d');
    }
    public function getUpdatedAtAttribute($date)
    {
        $enDate= explode(' ',Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans());
        $arDate[0] = $enDate[0];
        if( isset($enDate[2]) ){ $arDate[1] = $enDate[2];};
        $arDate[2] = $enDate[1];
        $newDate = implode(" ", $arDate);
        return $newDate;
//        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }
}
