<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    protected $table = 'transfers';
    public $timestamps = true;
    protected $fillable = array('cash','foundation_id','charity_id');

    public function foundation()
    {
        return $this->belongsTo('App\Models\Foundation');
    }

    public function charity()
    {
        return $this->belongsTo('App\Models\Charity');
    }

}
