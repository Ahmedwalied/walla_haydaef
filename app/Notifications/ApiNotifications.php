<?php

namespace App\Notifications;

use App\Models\Fund;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ApiNotifications extends Notification
{
    use Queueable;

    protected $fund;

    public function __construct(Fund $fund)
    {
        $this->fund = $fund;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function ToDatabase()
    {
        return [
            'id'=>$this->fund->id,
            'name'=> 'بمبلغ' .' '.$this->fund->cash.' ' . 'ريال'. ' '.'من رصيد محفظنك',
            'data' =>   'تم التبرع لمشروع'.' '.$this->fund->charity->name,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
