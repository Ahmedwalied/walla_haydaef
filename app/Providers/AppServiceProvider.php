<?php

namespace App\Providers;

use App\Models\City;
use App\Models\Course;
use App\Models\Institute;
use App\Models\SiteInformation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        app()->singleton('lang',function(){
            return session()->get('lang');
        });

        Schema::defaultStringLength(191);

//        view()->composer('layouts.app', function ($view){
//            $view->with('info', SiteInformation::find(1));
//        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
