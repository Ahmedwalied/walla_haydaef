<?php

namespace App;



use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role','bio','user_name','phone','cash','image','app_cash'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function funds()
    {
        return $this->hasMany('App\Models\Fund');
    }



//
//    public function permissions()
//    {
//        return $this->belongsToMany(Permission::class);
//    }
//
//    public function have_permission($permission_title)
//    {
//        $permission = Permission::where('title',$permission_title)->first();
//        $user_permission = PermissionUser::where('user_id',auth()->id())->where('permission_id',$permission->id)->first();
//
//        if ($user_permission){
//            return true;
//        }
//        else{
//            return false;
//        }
//    }


//    public function links ()
//    {
//        return $this->hasMany(Link::class);
//    }

}
