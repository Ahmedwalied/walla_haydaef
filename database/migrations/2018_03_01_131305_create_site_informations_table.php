<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_informations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('site_name');
            $table->string('site_description');
//            $table->string('site_video');
//            $table->string('site_logo');
            $table->string('site_tags');
            $table->string('telephone');
            $table->string('email');
//            $table->string('address');
            $table->string('map');
//            $table->string('work_hours');
            $table->string('paypal_mode');
            $table->string('paypal_user');
            $table->string('paypal_pass');
            $table->string('paypal_signature');
            $table->string('facebook');
            $table->string('twitter');
            $table->string('linkedin');
            $table->string('youtube');
            $table->string('instgram');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_informations');
    }
}
