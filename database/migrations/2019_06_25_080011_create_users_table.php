<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('user_name')->nullable();
            $table->string('phone');
            $table->double('app_cash')->default(0);
            $table->double('cash')->default(0);
            $table->string('email')->nullable();
            $table->string('password');
            $table->string('image')->nullable();
            $table->text('bio')->nullable();
            $table->tinyInteger('user_type')->default(0);  //  إدارة 2 - المالك 1 -  عضو عادي 0-;
            $table->tinyInteger('is_active')->default(0);
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
