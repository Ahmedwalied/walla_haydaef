<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchesTable extends Migration {

	public function up()
	{
		Schema::create('branches', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->unsignedInteger('foundation_id');
			$table->string('name', 255);
			$table->string('address', 255);
		});
	}

	public function down()
	{
		Schema::drop('branches');
	}
}
