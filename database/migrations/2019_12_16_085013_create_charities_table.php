<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharitiesTable extends Migration {

	public function up()
	{
		Schema::create('charities', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->unsignedInteger('foundation_id');
			$table->string('name', 255);
			$table->string('address', 255);
			$table->longText('description');
            $table->boolean('status')->default(1);
			$table->double('cash')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('charities');
	}
}
