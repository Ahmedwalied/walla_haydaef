<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFoundationsTable extends Migration {

	public function up()
	{
		Schema::create('foundations', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name', 255);
			$table->string('address', 255);
			$table->longText('about_us');
			$table->string('manager_name', 255);
			$table->boolean('status')->default(1);
			$table->double('cash')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('foundations');
	}
}
