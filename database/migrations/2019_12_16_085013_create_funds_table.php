<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFundsTable extends Migration {

	public function up()
	{
		Schema::create('funds', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('user_id');
			$table->integer('charity_id');
			$table->double('cash')->default(0);
			$table->double('to_be_paid')->default(0);
		});
	}

	public function down()
	{
		Schema::drop('funds');
	}
}
