<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Traits\HasRoles;

class AdminPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    use HasRoles;

    protected $guard_name = 'web';

    public function run()
    {
        $user = \App\User::findOrFail(1);
        $permissions = \Spatie\Permission\Models\Permission::pluck('name')->toArray();
        $user->syncPermissions($permissions);
    }
}
