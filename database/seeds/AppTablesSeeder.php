<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('app_tables')->insert([
           //  ["title"=>"features",'title_ar'=>'مميزات الموقع','is_active'=>1],

         //   ["title"=>"cities",'title_ar'=>'المدن','is_active'=>1],
            ["title"=>"users",'title_ar'=>'الأعضاء','is_active'=>1],
            ["title"=>"settings",'title_ar'=>'الإعدادات','is_active'=>1],
            ["title"=>"categories",'title_ar'=>'التصنيفات','is_active'=>1],
            ["title"=>"sliders",'title_ar'=>'السلايدر','is_active'=>1],
            ["title"=>"pages",'title_ar'=>'الصفحات','is_active'=>1],
            ["title"=>"contacts",'title_ar'=>'تواصل معنا','is_active'=>1],
            ["title"=>"notifications",'title_ar'=>'الإشعارات','is_active'=>1],
            ["title"=>"permissions",'title_ar'=>'الصلاحيات','is_active'=>1],
            ["title"=>"roles",'title_ar'=>'الأدوار','is_active'=>1],
            ["title"=>"banks",'title_ar'=>'البنوك','is_active'=>1],
            ["title"=>"bank_transfers",'title_ar'=>'الحوالات البنكية','is_active'=>1],
            ["title"=>"orders",'title_ar'=>'الطلبات','is_active'=>1],
          //  ["title"=>"wallets",'title_ar'=>'المحفظة','is_active'=>1],
            ["title"=>"payment_methods",'title_ar'=>'طرق الدفع','is_active'=>1],
            ["title"=>"products",'title_ar'=>'المنتجات','is_active'=>1],
           // ["title"=>"traders",'title_ar'=>'طلبات التجار','is_active'=>1],
            ["title"=>"mailingList",'title_ar'=>'القائمة البريدية','is_active'=>1],





        ]);
    }
}
