<?php

use App\Model\AppTables;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['admin','writer'];
        foreach ($roles as $role)
        {
            Role::create(['name' => $role]);
        }

        $perms = AppTables::select('id','title')->where('is_active',1)->get();
//        $perms = ['create','read','update','delete'];
        foreach ($perms as $perm)
        {
            Permission::create(['name' => $perm->title . ' create']);
            Permission::create(['name' => $perm->title . ' read']);
            Permission::create(['name' => $perm->title . ' update']);
            Permission::create(['name' => $perm->title . ' delete']);
        }
    }
}
