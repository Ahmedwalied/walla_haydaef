<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
//use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'user_name' => 'admin',
            'email' => 'admin@domain.com',
            'phone' => '01060586792',
            'user_type' => 2,
            'is_active'=> 1,
            'password' => bcrypt('123456789'),
        ]);
//        $faker = Faker::create();
//        foreach (range(1,10) as $index) {
//            DB::table('users')->insert([
//                'name' => $faker->name,
//                'email' => $faker->email,
//                'phone' => $faker->phoneNumber,
//                'password' => bcrypt('secret'),
//                'city_id' => $faker->randomDigitNotNull
//            ]);
//        }
    }
}
