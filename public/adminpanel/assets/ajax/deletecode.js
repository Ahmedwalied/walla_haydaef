$(document).on('click','.deletecode-btn',function (e) {
    e.preventDefault();
    var btn = $(this);
    var id = btn.data("product");
    var code = btn.data("id");
    var url='/admin/products/'+ id;
    var token = $('meta[name="_token"]').attr('content');
    var quantity = $('input#quantity').val();
    $.ajax({
        type: 'DELETE',
        url: url,
        data: {
            'id' : id,
            "_method": 'DELETE',
            "_token": token,
            'deletecode': code,
        },
        success: function (data) {
            if (data.success) {
                sweetAlert("الحذف", data.message, "success");
                btn.parents('tr').remove();
                $('input#quantity').val(quantity -1);
            }else{
                console.log(data);
            }
        },
        error: function (xhr) {
            console.log(xhr.responseText);
        },
    });
});