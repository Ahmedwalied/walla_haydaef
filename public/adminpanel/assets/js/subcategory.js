$(document).ready(function () {
    $('#category_id').on('change', function (e) {
        console.log(e);
        var category_id = e.target.value;
        //console.log("category_id=" + category_id);
        if (category_id) {

            $.ajax({
                url: '/admin/products/create/ajax-city/' + category_id,
                type: "GET",

                dataType: "json",

                success: function (data) {
                    $('#subcategory_id').empty();
                    $('#subcategory_id').append('<option value=""> اختر القسم الفرعي</option>');
                    $.each(data, function (i, state) {
                        $('#subcategory_id').append('<option value="' + state.id + '">' + state.name_ar + '</option>');
                    });

                }

            });
        } else {
            $('#subcategory_id').empty();
        }
    });
});