<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'الرئيسية',
    'admin_panel' => 'لوحة التحكم ',
    'login' => 'تسجيل الدخول',
    'logout' => 'تسجيل الخروج',
    'remember' =>'تذكرنى',
    'password_reset' =>'نسيت كلمة المرور ',
    'contact_us' =>' اتصل بنا ',
    'contact_us_messages' =>'رسائل اتصل بنا ',
    'inbox' =>'رسائل البريد',
    'site_visit' =>'زيارة الموقع ',
    'mailing_list' =>'القائمة البريدية ',
    'site_setting' =>'اعدادات الموقع ',
    'about_us_edit' =>'تعديل صفحة عن التطبيق ',
    'site_info_edit' =>'تعديل معلومات الشركة ',
    'mailing_list_subscribers_contact' =>' مراسلة المشتركين ',

    'about'=>'من نحن ',
    'page_name'=>'اسم الصفحة ',
    'page_content'=>'محتوى الصفحة',
    'notifications'=>'الإشعارات ',
    'bio'=>'نبذة',




    'name' => 'الاسم',
    'email' => 'البريد الالكترونى',
    'pass' => 'كلمة المرور',
    'confirm_pass' => 'تأكيد كلمة المرور',
    'tel' => 'الهاتف',
    'image' => 'الصورة',
    'images' => 'الصور',
    'title' => 'العنوان',
    'description' => 'الوصف',
    'category' => 'القسم ',
    'article' => 'المقال',
    'article_content' => 'محتوى المقال',
    'author' => 'الكاتب',
    'message' => 'الرسالة',
    'message_title' => 'عنوان الرسالة',
    'message_content' => 'محتوى الرسالة',
    'content' => 'المحتوى',
    'created_at' => 'تاريخ الإنشاء',
    'updated_at' => 'تاريخ التعديل',
    'action' => 'الإجراء المتخذ',
    'delete' => 'حذف',
    'update' => 'تعديل',
    'add' => ' إضافة جديد',
    'send' => 'إرسال',

    'general' => 'عام',
    'countries' => 'المناطق',
    'country' => 'المنطقة',
    'cities' => 'المدن',
    'city' => 'مدينة',
    'population' => 'عدد السكان',
    'map' => 'رابط الخريطة',
    'video' => 'الفيديو',
    'videos' => 'الفيديوهات',

    'rate' => 'التقييم',
    'logo' => 'اللوجو',
    'discount' => 'الخصم',

    'site_info' => 'معلومات الموقع',
    'edit_site_info' => 'تعديل معلومات الموقع',
    'site_name' => 'اسم الموقع',
    'site_description' => 'وصف الموقع',
    'site_front_image'=>'صورة واجهة الموقع ',
    'site_video' => 'فيديو الموقع',
    'site_logo' => 'شعار الموقع',
    'site_tags' => 'الكلمات الدلالية الموقع',
    'address' => 'العنوان ',
    'work_hours' => 'أوقات العمل',
    'contact_methods' => 'وسائل الاتصال',
    'social_media' => 'وسائل التواصل الاجتماعى',


    'not_admin'=>'لابد ان تكون صاحب الموقع  لتتمكن من الدخول ',
    'admin_login'=>'تسجيل الدخول للوحة التحكم ',
    'users' => 'الأعضاء',
    'admin' => 'مدير الموقع',
    'admins' => 'الإدارة',
    'role' => 'الصلاحية',
    'roles' => 'الصلاحيات',

    'block' =>'حظر ',
    'disblock'=>'فك الحظر ',
    'suspend' =>'تعليق ',
    'dissuspend'=>'فك التعليق ',
    'show'=>'عرض',
    'show_at_home'=>'ظهور على الصفحة الرئيسية',

    'static_pages'=>'الصفحات الثابتة ',
    'invoice'=>'الفاتورة ',

    'customers'=>'العملاء',
    'customer'=>'العميل',

    'program_setting'=>'إعدادات البرنامج',
    'categories'=>'الأقسام ',
    'workers'=>'العمال ',
    'maintenances'=>'الصيانات',
    'maintenance'=>' الصيانة',
    'maintenance_categories'=>'أنواع الصيانة',
    'maintenance_category'=>'نوع الصيانة',
    'cost'=>'التكلفة',
    'maintenance_date'=>'تاريخ الصيانة',
    'expired_date'=>'تاريخ انتهاء مدة الصلاحية',
    'banks'=>'البنوك',
    'contact-us'=>'اتصل بنا',
    'POS'=>'الفروع  ',
    'notes'=>'ملاحظات',

    'expires'=>'فترة صلاحية البطاقات',
    'period'=>'فترة الصلاحية',
    'in_days'=>'بالأيام',
    'free'=>'إرسال أكواد (بطاقات ) مجانية',
    'yes'=>'نعم',
    'no'=>'لا',

    'app_setting'=>'إعدادات الموقع',
    'bankAccounts' => 'الحسابات البنكية',
    'bank_account_data'=>'بيانات الحساب البنكى للشركة',
    'bank_name' => 'اسم البنك',
    'bank_account_number' => 'رقم الحساب',
    'bank_account_owner' => 'اسم صاحب الحساب',
    'iban' => 'ايبان',

    'register_period'=>'بيانات تاريخ التسجيل',
    'date_from'=>'من تاريخ',
    'date_to'=>'الى تاريخ',

    'register_message'=>'رسالة التسجيل',
    'url'=>'رابط الفيديو',
    'display_status'=>'خيارات نشر الفيديو',


    'employees'=>'المناديب',
    'id_no'=>'رقــم الهوية',
    'emp_no'=>'رقــم المندوب',
    'manager'=>'مدير عام',
    'operation'=>'مدير العمليات',
    'playback'=>'التشغيل',
    'accounts'=>'الحسابات',
    'edit'=>'صفحة تعديل عامة',
    'active'=>'نشط ',
    'user_name'=>'اسم المستخدم',

    'employee'=>'المندوب',
    'tasks'=>'مهام المناديب',
    'start_date'=>'تاريخ بداية المهمة',
    'alert_date'=>'تاريخ التذكير',
    'priority'=>'الأهمية',
    'complete_percentage'=>'نسبة الإنتهاء',

    'cars'=>'السيارات',
    'price'=>'السعر',
    'choose'=>'اختر من فضلك',
    'choose_country'=>'اختر المنطقة',
    'choose_city'=>'اختر المدينة',
    'code'=>'كود البطاقة',
    'status'=>'حالة البطاقة',
    'user'=>'صاحب البطاقة',

    'all_cards'=>'كل البطاقات',
    'active_cards'=>'البطاقات النشطة',
    'inactive_cards'=>'البطاقات الغير النشطة',
    'stopped_cards'=>'البطاقات المتوقفة',
    'advertised_cards'=>'البطاقات المعلن عنها',
    'sold_cards'=>'البطاقات المباعة',
    'expired_cards'=>'البطاقات المنتهية',

    'add_cards'=>'إضافة عدد من البطاقات',
    'number'=>'عدد البطاقات',

    'balance'=>'الرصيد',
    'currency'=>'العملة',
    'serial'=>'رقم السريال',
    'not_used'=>'لم يستخدم بعد',

    'subscribers'=>'المشتركين',
    'subscriber'=>'مشترك',

    'ID_Number'=>'رقم الهوية / الإقامة',
    'Emp_number'=>'الرقم الوظيفى',

    'pay_orders'=>'مشتريات المشتركين',
    'card_name'=>'اسم البطاقة',
    'card_image'=>'صورة البطاقة',
    'pay_status'=>'حالة الدفع',
    'pay_method'=>'طريقة الدفع',


    'car'=>'السيارة',
    'branch'=>'الفرع',
    'transmission_type'=>'نوع محول الحركة',
    'engine_power'=>'قوة المحرك',
    'model_year'=>'سنة التصنيع',
    'pessengers_no'=>' عدد الركاب',
    'doors_no'=>' عدد الأبواب',
    'drivers'=>'السائقون',
    'driver'=>'السائق',
    'license_no'=>'رقم رخصة القيادة',
    'has_car'=>'يملك سيارة',

    'orders'=>' طلبات الحجز',
    'customer_name'=>' اسم العميل ',
    'reservation_start'=>' بداية الحجز',
    'reservation_end'=>' نهاية الحجز',
    'flight_no'=>' رقم الرحلة',
    'contact_type'=>'نوع التواصل',


    'foundations'=>'المؤسسات',
    'branches'=>'الفروع',
    'foundation_name'=>'اسم المؤسسه',
    'branch_name'=>'اسم الفرع',
    'charities'=>'المشاريع الخيريه',
    'cash'=>'المبلغ',
    'on'=>'مفتوح',
    'off'=>'مغلق',
    'details'=>'التفاصيل',
    'foundation_status'=>'حاله المؤسسه',
    'open'=>'مفتوح',
    'close'=>'مغلق',
    'desActive'=>'غير نشط',
    'not_found'=>'لايوجد',
    'finished'=>'اكتمل',
    'phone'=>'رقم الهاتف',
    'wallet'=>'المحفظه',





];
