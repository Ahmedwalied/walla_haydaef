<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'institute_dashboard' => 'لوحة تحكم المعهد',
    'profile_update' => 'تعديل الملف الشخصى ',
    'institute_courses' => 'دورات المعهد ',
    'institute_living' => 'خيارات السكن المتوفرة ',
    'institute_airport' => 'خيارات استقبال المطار ',
    'not_login' =>'قم بتسجيل الدخول لكى تتمكن من الدخولة للوحة تحكم المعهد الخاص بك ',
    'block'=>'لقد تم حظرك من قبل ادارة الموقع',


];
