<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'add_success' => 'تمت الاضافة بنجاح',
    'update_success' => ' تم التعديل بنجاح',
    'delete_success' => ' تم الحذف بنجاح',
    'block_success' => 'تم حظر العضو بنجاح ',
    'disblock_success' => 'تم فك الحظر عن العضو بنجاح ',
    'suspend_success' => 'تم تعليق السيارة بنجاح ',
    'dissuspend_success' => 'تم الغاء تعليق السيارة بنجاح ',
    'errors_occur' =>'حدثت بعض الاخطاء التالية',
    'message_sent'=>'تم ارسال رسالتك بنجاح ',
    'send_mailing_list_success' =>'تم الارسال الى مشتركى القائمة البريدية بنجاح',
    'add_bank_transfer_success'=>'تم الحجز بنجاح يرجى انتظار موافقة الادارة وسيتم ارسال الفاتورة الى البريد الالكترونى الخاص بكم ',
    'activate_subscription'=>'تم تفعيل الاشتراك وارسال الفاتورة الى البريد الالكترونى للطالب',


];
