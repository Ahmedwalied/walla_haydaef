<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'home' => 'Home',
    'admin_panel' => 'Admin panel',
    'login' => 'Login',
    'logout' => 'Logout',
    'remember' =>'Remember',
    'password_reset' =>'Password reset ',
    'contact_us' =>'Contact us ',
    'inbox' =>'Inbox messages ',
    'site_visit' =>'Visit your site ',
    'mailing_list' =>'Mailing list ',
    'site_setting' =>'Site setting ',
    'about_us_edit' =>'Edit about us page',
    'site_info_edit' =>'Edit site information ',
    'mailing_list_subscribers_contact' =>' subscribers contact ',

    'about'=>'About us ',
    'page_name'=>'Page Name',
    'page_content'=>'Page Content',
    'notifications'=>'Notifications ',
    'bio'=>'bio',


    'name' => 'Name',
    'email' => 'Email',
    'pass' => 'Password ',
    'confirm_pass' => 'Confirm Password',
    'tel' => 'Telephone',
    'title' => 'Title',
    'description' => 'Description',
    'image' => 'Image',
    'images' => 'Images',
    'category' => 'Category',
    'article' => 'Article',
    'article_content' => 'Article content',
    'author' => 'Author',
    'message' => 'Message',
    'message_title' => 'Message title',
    'message_content' => 'Message content',
    'content' => 'Content',
    'created_at' => 'Created at',
    'updated_at' => 'Updated at',
    'action' => 'Action',
    'delete' => 'Delete',
    'update' => 'Update',
    'add' => 'Add New',
    'send' => 'Send',

    'general' => 'General',
    'countries' => 'Countries',
    'country' => 'Country',
    'cities' => 'Cities',
    'city' => 'City',
    'population' => 'Population',
    'map' => 'Map url',
    'video' => 'Video',
    'videos' => 'Videos',

    'rate' => 'Rate',
    'logo' => 'Logo',


    'site_info' => 'Site info ',
    'edit_site_info' => 'Site info update',
    'site_name' => 'Site name',
    'site_description' => 'Site description ',
    'site_front_image'=>'Site front image',
    'site_video' => ' Site video',
    'site_logo' => 'Site logo',
    'site_tags' => 'Site keywords',
    'address' => 'Office address',
    'work_hours' => 'Work hours',
    'contact_methods' => 'Contacts',
    'social_media' => 'Social media',

    'not_admin'=>'You Should Be Admin To login ',
    'admin_login'=>'Admin Panel Login ',
    'users' => 'Users',
    'admin' => 'Admin',
    'admins' => 'Admins',
    'role' => 'Role',
    'roles' => 'Roles',

    'block' =>'Block ',
    'disblock'=>'Disblock ',
    'suspend' =>'Suspend ',
    'dissuspend'=>'Cancel suspend ',
    'show'=>'Show',
    'show_at_home'=>'Show at home page',

    'static_pages'=>'Static pages',
    'invoice'=>'Invoice',

    'customers'=>'Customers',
    'customer'=>'Customer',

    'program_setting'=>'Program setting',
    'categories'=>'Categories',
    'workers'=>'Workers ',
    'maintenance'=>'Maintenance',
    'maintenances'=>' Maintenances',
    'maintenance_categories'=>'Maintenance Categories',
    'maintenance_category'=>'Maintenance Category',
    'cost'=>'Cost',
    'maintenance_date'=>'Maintenance date',
    'expired_date'=>'Expired date',
    'banks'=>'Banks',
    'contact-us'=>'contact-us',
    'POS'=>'Points Of Sales',
    'notes'=>'Notes',

    'expires'=>'Card validity period',
    'period'=>'expire',
    'in_days'=>'in Days',
    'free'=>'Send free codes',
    'yes'=>'yes',
    'no'=>'no',

    'app_setting'=>'Application setting',
    'bank_account_data'=>'Bank account data',
    'bankAccounts' => 'Bank Accounts',
    'bank_name' => 'Bank Name',
    'bank_account_number' => 'Bank Account Number',
    'bank_account_owner' => 'Bank Account Owner Name',
    'iban' => 'Iban',
    'swift' => 'Swift',

    'register_period'=>'Registration date data',
    'date_from'=>'From the date of',
    'date_to'=>'To date',

    'register_message'=>'Registration message',
    'url'=>'Video Url',
    'display_status'=>'Video display options',

    'employees'=>'employees',
    'id_no'=>'ID number',
    'emp_no'=>'Employee Number',
    'manager'=>'manager',
    'operation'=>'operation manager ',
    'playback'=>'playback',
    'accounts'=>'accounts',
    'edit'=>'General editing page',
    'active'=>'Active ',
    'user_name'=>'user name',

    'employee'=>'employee',
    'tasks'=>'employee tasks',
    'start_date'=>'start date',
    'alert_date'=>'alert date',
    'priority'=>'priority',
    'complete_percentage'=>'complete percentage',

    'cars'=>'cars',
    'price'=>'price',
    'choose'=>'please choose',
    'code'=>'card codeة',
    'status'=>'card status',
    'user'=>'card user',

    'all_cards'=>'All cars',
    'active_cards'=>'Active cars',
    'inactive_cards'=>'Inactive cars',
    'stopped_cards'=>'Stopped cars',
    'advertised_cards'=>'Advertised cars',
    'sold_cards'=>'Sold cars',
    'expired_cards'=>'Expired cars',

    'add_cards'=>'Add multiple cars',
    'number'=>'Cards number',


    'balance'=>'balance',
    'currency'=>'currency',
    'serial'=>'serial number',
    'not_used'=>'not used',

    'subscribers'=>'subscribers',
    'subscriber'=>'subscriber',

    'ID_Number'=>'ID Number',
    'Emp_number'=>'Employer number',

    'pay_orders'=>'pay orders',
    'card_name'=>'card name',
    'card_image'=>'card image',
    'pay_status'=>'pay status',
    'pay_method'=>'pay method',

    'car'=>'car',
    'branch'=>'branch',
    'transmission_type'=>'transmission type',
    'engine_power'=>'engine power ',
    'model_year'=>'model year',
    'pessengers_no'=>' pessengers number ',
    'doors_no'=>' doors number ',

    'drivers'=>'drivers',
    'driver'=>'driver',
    'license_no'=>'licence number ',
    'has_car'=>'has car ',


    'orders'=>'orders',
    'customer_name'=>'customer name ',
    'reservation_start'=>'reservation start',
    'reservation_end'=>'reservation end',
    'flight_no'=>'flight number',
    'contact_type'=>'Contact type',

    'foundations'=>'Foundations',
    'branches'=>'Branches',
    'foundation_name'=>'Foundation Name',
    'charities'=>'Charities',
    'branch_name'=>'Branch Name',
    'cash'=>'Cash',
    'on'=>'On',
    'off'=>'Off',
    'details'=>'Details',
    'foundation_status'=>'Foundation Status',
    'open'=>'Open',
    'close'=>'Close',
    'desActive'=>'DesActive',
    'not_found'=>'Not Found',
    'finished'=>'Finished',
    'phone'=>'Phone',
    'wallet'=>'Wallet',


];
