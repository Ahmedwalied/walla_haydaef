<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'institute_dashboard' => 'Institute Dashboard',
    'profile_update' => 'Profile Update ',
    'institute_courses' => 'Institute Courses ',
    'institute_living' => 'Available Accommodation Options ',
    'institute_airport' => 'Available Airport Pickup Options ',
    'not_login' =>'Please login to can login to your institute dashboard ',
    'block'=>'You are blocked by admin',


];
