<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'add_success' => 'Successfully added',
    'update_success' => 'Successfully Updated',
    'delete_success' => 'Successfully Deleted',
    'block_success' => 'Successfully Blocked',
    'disblock_success' => 'Successfully Disblocked',
    'errors_occur' =>'Some errors occur',
    'message_sent'=>'Your message sent successfully ',
    'send_mailing_list_success' =>'Sending to mailing list users successfully ',
    'add_bank_transfer_success'=>'Your booking has been successfully completed. Please wait for the approval of the administration and the invoice will be sent to your email address ',
    'activate_subscription'=>'Subscription activated and invoice sent to student email',


];
