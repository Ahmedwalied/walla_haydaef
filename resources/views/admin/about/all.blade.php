@extends('admin.adminLayout.App')


@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold"> {{trans('admin.home')}} - </span>{{trans('admin.about')}}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active"><i class="icon-archive"></i> {{ trans('admin.static_pages')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area ////////////////-->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <a href="/admin/about/create" class="btn btn-info btn-lg" role="button">{{trans('admin.add')}}</a>
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{trans('admin.static_pages')}} </h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table class="table datatable-save-state" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr>
                                    <th class="text-center">{{trans('admin.page_name')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Element -->
                                @foreach($pages as $page)
                                    <tr >
                                        <td class="text-center"> <a href="{{'/admin/about/'.$page->id.'/edit' }}">{{ $page->translation->first()->title }}</a></td>
                                    </tr>
                                    <!-- Element End -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- Content area ////////////////-->
    <!-- Dashboard content -->

@endsection
