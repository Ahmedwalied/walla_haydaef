@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{ isset($about) ? trans('admin.update') : trans('admin.add')}}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/about"><i class="icon-archive position-left"></i> {{trans('admin.static_pages')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i>{{ isset($about) ? trans('admin.update') : trans('admin.add')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ isset($about) ? trans('admin.update') : trans('admin.add')}}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal form-validate-jquery" action="/admin/about{{ isset($about) ? '/'. $about->id : '' }}" method="post"
                              novalidate="novalidate" enctype="multipart/form-data">
                            {!! isset($about) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            {{ csrf_field() }}
                            <fieldset class="content-group">
                                <div >
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#general">{{trans('admin.general')}}</a></li>
                                        @foreach ( \Illuminate\Support\Facades\Config::get('languages') as $lang => $language)
                                            <li><a data-toggle="tab" href="#{{$lang}}">{{trans($language)}}</a></li>
                                        @endforeach
                                    </ul>

                                    <div class="tab-content">
                                        <div id="general" class="tab-pane fade in active">
                                            <!-- /basic text input -->
                                            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                                <label class="col-lg-3 control-label">{{trans('admin.image')}} </label>
                                                <div class="col-lg-9">
                                                    <input type="file" class="file-input" data-browse-class="btn btn-primary btn-block"
                                                           data-show-remove="false" data-show-caption="false" data-show-upload="false"
                                                           value="" name="image">
                                                    @include('layouts.error', ['input' => 'image'])
                                                </div>
                                            </div>
                                            <!-- End Edite -->
                                        @if(isset($about))
                                            <!-- /basic text input -->
                                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                                    <div class="col-lg-3"></div>
                                                    <div class="col-lg-9">
                                                        <img src="/images/about/{{$about->image}}" class="img-responsive center-block">
                                                    </div>
                                                </div>
                                        @endif
                                        <!-- End Edite -->
                                        </div>

                                        @foreach ( \Illuminate\Support\Facades\Config::get('languages') as $lang => $language)
                                            <div id="{{$lang}}" class="tab-pane fade ">
                                                <!-- Basic text input -->
                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">{{trans('admin.page_name')}} [{{trans($language)}}]<span
                                                                class="text-danger">*</span></label>
                                                    <div class="col-lg-9">
                                                        <input type="text" name="{{$lang}}_title" class="form-control" required="required"
                                                               placeholder="{{trans('admin.page_name')}}" aria-required="true" value="{{ isset($about) ? $about->translation($lang)->first()->title  : old($lang.'_title') }}">
                                                        @include('layouts.error', ['input' => $lang.'_title'])
                                                    </div>
                                                </div>
                                                <!-- /basic text input -->

                                                <div class="form-group">
                                                    <label class="control-label col-lg-3">{{trans('admin.page_content')}} [{{trans($language)}}] <span
                                                                class="text-danger">*</span></label>
                                                    <div class="col-lg-9">
                                                        <textarea name="{{$lang}}_description"  rows="10" cols="125" required="required">{{ isset($about) ? $about->translation($lang)->first()->description : old($lang.'_description') }}</textarea>
                                                        @include('layouts.error', ['input' => $lang.'_description'])
                                                    </div>
                                                </div>

                                            </div>
                                        @endforeach


                                    </div>

                                </div>
                            </fieldset>
                            <div class="text-left">
                                <button type="submit" class="btn btn-primary"><i
                                            class=" icon-arrow-right7 position-left"></i>{{ isset($about) ? trans('admin.update') : trans('admin.add')}}
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection


