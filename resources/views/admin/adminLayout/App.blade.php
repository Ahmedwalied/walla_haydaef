<!DOCTYPE html>

@if(app()->getLocale()=='en')
	<link href="{{asset('/assets/css/custem-en.css')}}" rel="stylesheet" type="text/css">
@endif

<html lang="{{app()->getLocale()=='ar'?'ar':'en'}}" dir="{{app()->getLocale()=='ar'?'rtl':'ltr'}}">
@include('admin.adminLayout.header')
<body>
	<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<a class="navbar-brand" href="https://www.ibtdi.com/" target="_blank">
				<img src="{{url('adminpanel/assets/images/ibtdi.png')}}" alt="logo" style="display: inline-block;">
				<span>لوحة تحكم تطبيق والله هيضاعف</span>
			</a>
			<ul class="nav navbar-nav">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">


{{--			<p class="navbar-text"><span class="label bg-success-400">{{Auth::user()->email}}</span></p>--}}
			<p class="navbar-text"><span class="label bg-success-400">اون لاين</span></p>

			<ul class="nav navbar-nav navbar-right">




				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-flag3"></i>
						<span class="visible-xs-inline-block position-right">{{trans('admin.notifications')}}</span>
						<span class="badge bg-warning-400">@if(isset(Auth::user()->unreadNotifications)){{count(Auth::user()->unreadNotifications)}} @else 0 @endif</span>
					</a>
					<div class="dropdown-menu dropdown-content width-350">
						<div class="dropdown-content-heading">
							{{trans('admin.notifications')}}
							<ul class="icons-list">
								<li><a href="{{url('/admin/notifications')}}"><i class="icon-square-left"></i></a></li>
							</ul>
						</div>

						<ul class="media-list dropdown-content-body">
							@if(count(Auth::user()->unreadNotifications) > 0)
							@foreach(Auth::user()->unreadNotifications as $note)
							<li class="media">
								<div class="media-left">
									<img src="{{asset('/adminpanel/assets/images/placeholder.jpg')}}" class="img-circle img-sm" alt="">
								</div>

								<div class="media-body">
									<a href="{{url('admin')}}/{{isset($one->data['url']) ? $one->data['url'] : 'notifications'}}" class="media-heading">
										<span class="text-semibold">{{ $note->data['data'] }}</span>
										<span class="media-annotation pull-right">{{$note->created_at->diffforhumans()}}</span>
									</a>
									<span class="media-annotation">{{$note->data['data'] .' '. $note->data['name'] }}</span>
								</div>
							</li>
							@endforeach
							@else
							<li class="media text-center">
							لا يوجد اشعارات جديدة
							</li>
							@endif

						</ul>

						<div class="dropdown-content-footer">
							<a href="{{url('/admin/notifications')}}" data-popup="tooltip" title="كل الإشعارات"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>

{{--				<li class="dropdown">--}}
{{--					<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--						<i class="fa"><img src="{{asset('images/'.app()->getLocale().'.png')}}"></i> {{ trans(\Illuminate\Support\Facades\Config::get('languages')[app()->getlocale()]) }}--}}
{{--					</a>--}}
{{--					<ul class="dropdown-menu">--}}
{{--						@foreach ( \Illuminate\Support\Facades\Config::get('languages') as $lang => $language)--}}
{{--							@if ($lang != app()->getlocale())--}}
{{--								<li>--}}
{{--									<a href="{{ route('lang.switch', $lang) }}"><i class="fa"><img src="{{asset('images/'.$lang.'.png')}}"></i> {{trans($language)}}</a>--}}
{{--								</li>--}}
{{--							@endif--}}
{{--						@endforeach--}}
{{--					</ul>--}}
{{--				</li>--}}

                    <!-- #END# Call Search -->
{{--				<li><a  class="dropdown-item" href="{{url('/')}}" target="_blank"><i class="icon-home2"></i> زيارة الموقع  </a></li>--}}
{{--				</li>--}}
				<li><a  class="dropdown-item" href="{{ route('logout') }}"
						onclick="event.preventDefault();
						 document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> {{trans('admin.logout')}}</a></li>
				</li>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					@csrf
				</form>

			</ul>
		</div>
	</div>
	<div class="page-container">

		<div class="page-content">

			<!-- Main sidebar -->
		@include('admin.adminLayout.sidebar')
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{trans('admin.admin_panel')}}</span></h4>
						</div>
					</div>


				</div>
				<div class="content">
				{{--@include('admin.message')--}}
				@yield('content')
					<hr>
				<div class="footer text-muted">
					<a href="https://www.ibtdi.com/">لوحة تحكم </a> من <a href="https://www.ibtdi.com/" target="_blank">إبتدي</a>© {{date('Y')}}.
				</div>
				</div>
			</div>
		</div>

	</div>
	<!-- /page container -->
	<script src="{{url('adminpanel/assets/js/fastselect.standalone.min.js')}}"></script>
	<script type="text/javascript" src="{{url('adminpanel/assets/js/main-js.js')}}"></script>
	<script type="text/javascript" src="{{url('adminpanel/assets/js/subcategory.js')}}"></script>
	<script type="text/javascript" src="{{url('adminpanel/assets/js/new-js.js')}}"></script>


</body>
</html>
