<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="_token" content="{{ csrf_token() }}"/>

    <title>لوحة تحكم{{ config('app.name', 'Laravel') }} | @yield('title')</title>
    <link rel="shortcut icon" href="{{ asset('adminpanel/assets/images/favicon.png') }}">
	<!-- Global stylesheets -->
	<link href="{{url('adminpanel/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('adminpanel/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('adminpanel/assets/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('adminpanel/assets/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('adminpanel/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('adminpanel/assets/css/style.css')}}" rel="stylesheet" type="text/css">
	<link href="{{url('adminpanel/assets/css/tabs.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('adminpanel/dist/fastselect.min.css')}}" rel="stylesheet" type="text/css">

    @yield("styles")
    <!-- Core JS files -->
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/libraries/bootstrap.min.js')}}"></script>

    <!--Sweet Alert style-->
    <link rel="stylesheet" type="text/css" href="{{url('adminpanel/dist/sweetalert.css')}}">
    <link rel="stylesheet" href="{{url('adminpanel/assets/css/font-awesome.min.css')}}" type="text/css" />
    <!--Sweet Alert Style-->
    <!--Sweet Alert Js-->
    <script src="{{url('adminpanel/dist/sweetalert.min.js')}}"></script>
    <!--Sweet Alert Js-->
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/loaders/blockui.min.js')}}"></script>

    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/libraries/jquery_ui/core.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/libraries/jquery_ui/full.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/components_modals.js') }}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/tables/datatables/datatables.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/datatables_basic.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/form_select2.js')}}"></script>
    <link rel="stylesheet" href="{{url('adminpanel/assets/bootstrap-select/bootstrap-select.min.css')}}">
    <script src="{{url('adminpanel/assets/bootstrap-select/bootstrap-select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/datepicker.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/selects/selectboxit.min.js')}}"></script>
    @yield("scripts")

</head>