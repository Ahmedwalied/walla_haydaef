<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="/admin" class="media-left"><img src="{{url('/images/logo.png')}}" class="img-circle img-sm"></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold">{{auth()->user()->name}}</span>
                        <div class="text-size-mini text-muted">
                            <span class="media-heading text-semibold small-txt">والله هيضاعف</span>
                        </div>
                    </div>
                    @can('settings read')
                    <div class="media-right media-middle">
                        <ul class="icons-list">
                            <li>
                                <a href="/admin/settings"><i class="icon-cog3"></i></a>
                            </li>
                        </ul>
                    </div>
                    @endcan
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <li @if(Request::is('admin')) class="active" @endif><a href="/admin"><i class="icon-home4"></i> <span>{{trans('admin.home')}}</span></a>
                    </li>


                    @can('users read')
                        <li>
                            <a href="#"><i class="icon-users"></i> <span>الأعضاء و المدراء</span></a>
                            <ul>
                                {{--<li @if(Request::is('admin/users/create')) class="active" @endif><a href="/admin/users/create">أضف عضو للإدارة</a></li>--}}
                                <li @if(Request::is('admin/users')) class="active" @endif><a href="/admin/users">قائمة الأعضاء</a></li>
                                {{--<li @if(Request::is('admin/companies')) class="active" @endif><a href="/admin/companies">قائمة الشركات</a></li>--}}
                                <li @if(Request::is('admin/users-roles')) class="active" @endif><a href="/admin/users-roles">قائمة المدراء</a></li>
                            </ul>
                        </li>
                    @endcan
                    @can('roles read')
                        <li @if(Request::is('permissions')) class="active" @endif>
                            <a href=""><i class="icon-pencil7"></i> <span>الصلاحيات والأدوار</span></a>
                            <ul>
{{--                                <li @if(Request::is('admin/roles/create')) class="active" @endif><a href="/admin/roles/create">أضف دور</a></li>--}}
                                <li @if(Request::is('admin/roles')) class="active" @endif><a href="/admin/roles">الأدوار</a></li>
                            </ul>
                        </li>
                    @endcan
{{--                    @can('categories read')--}}
{{--                        <li @if(Request::is('admin/categories')) class="active" @endif>--}}
{{--                            <a href="/admin/categories"><i class="icon-anchor"></i> <span>{{trans('admin.categories')}}</span></a>--}}
{{--                        </li>--}}
{{--                    @endcan--}}
                    @can('users read')
                        <li @if(Request::is('admin/foundations')) class="active" @endif>
                            <a href="/admin/foundations"><i class="icon-home"></i> <span>{{trans('admin.foundations')}}</span></a>
                        </li>
                    @endcan
                    @can('users read')
                        <li @if(Request::is('admin/charities')) class="active" @endif>
                            <a href="/admin/charities"><i class="icon-home2"></i> <span>{{trans('admin.charities')}}</span></a>
                        </li>
                    @endcan
                    @can('about read')
                        <li @if(Request::is('admin/about')) class="active" @endif>
                            <a href="/admin/about"><i class="icon-archive"></i> <span>{{trans('admin.static_pages')}}</span></a>
                        </li>
                    @endcan
                    @can('contacts read')
                        <li @if(Request::is('admin/contact-us')) class="active" @endif>
                            <a href="/admin/contact-us"><i class="icon-phone"></i> <span>{{trans('admin.contact-us')}} </span></a>
                        </li>
                    @endcan
                    @can('pages read')
                        <li @if(Request::is('admin/pages')) class="active" @endif>
                            <a href="/admin/pages"><i class="icon-stack"></i> <span>الصفحات الثابته</span></a>
                        </li>
                    @endcan
                    @can('notifications read')
                        <li @if(Request::is('admin/notifications')) class="active" @endif>
                            <a href="/admin/notifications"><i class="icon-flag3"></i> <span>{{trans('admin.notifications')}}</span></a>
                        </li>
                    @endcan


                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
