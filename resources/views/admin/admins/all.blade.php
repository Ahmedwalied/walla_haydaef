@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold"> {{trans('admin.home')}} - </span>{{trans('admin.admins')}}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active"><i class="icon-users position-left"></i> {{trans('admin.admins')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area ////////////////-->
    <div class="content">
    @include('layouts.message')
        <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <a href="/admin/admins/create" class="btn btn-info btn-lg" role="button"><i class="icon-user-plus"></i>{{trans('admin.add')}}</a>
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{trans('admin.admins')}} </h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table  class="table datatable-save-state" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr>
                                    <th class="text-center">id</th>
                                    <th class="text-center">{{trans('admin.name')}}</th>
                                    <th class="text-center">{{trans('admin.role')}}</th>
                                    <th class="text-center">{{trans('admin.email')}}</th>
                                    <th class="text-center">{{trans('admin.created_at')}}</th>
                                    <th class="text-center">{{trans('admin.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Element -->
                                @foreach($users as $user)
                                    <tr parent_id="{{ $user->id }}">
                                        <td class="text-center">{{ $user->id }}</td>
                                        <td class="text-center"> <a href="{{'/admin/admins/'.$user->id.'/edit' }}">{{ $user->name }}</a></td>
                                        <td class="text-center">{{ $user->user_type }}</td>
                                        <td class="text-center">{{ $user->email }}</td>
                                        <td class="text-center">{{ $user->created_at }}</td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="text-primary-600">
                                                    <a href="{{'/admin/users/'.$user->id.'/edit' }}"
                                                       title="{{trans('admin.update')}} "><i class="icon-pencil7"></i>
                                                    </a>
                                                </li>
                                                <li class="text-danger-600">
                                                    <a onclick="return false;" token="{{ csrf_token() }}"
                                                       object_id="{{ $user->id }}"
                                                       delete_url="/admin/users/{{$user->id }}"
                                                       class="sweet_warning" href="#" title="{{trans('admin.delete')}} "><i
                                                                class="icon-trash"></i>
                                                    </a>
                                                </li>

                                            </ul>
                                        </td>
                                    </tr>
                                    <!-- Element End -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- Content area ////////////////-->
    <!-- Dashboard content -->

@endsection