@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{ isset($user) ? trans('admin.update') : trans('admin.add') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/admins"><i class="icon-users position-left"></i> {{trans('admin.admins')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ isset($user) ? trans('admin.update') : trans('admin.add') }} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ isset($user) ? trans('admin.update') : trans('admin.add') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal form-validate-jquery" action="/admin/admins{{ isset($user) ? '/'. $user->id : '' }}" method="post"
                              novalidate="novalidate" enctype="multipart/form-data" >
                            {!! isset($user) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            {{ csrf_field() }}
                            <!-- Basic text input -->
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.name')}} <span
                                                class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input type="text" name="name" class="form-control" required="required"
                                               placeholder="{{trans('admin.name')}}" aria-required="true" value="{{ isset($user) ? $user->name  : old('name') }}">
                                        @include('layouts.error', ['input' => 'name'])
                                    </div>
                                </div>
                                <!-- /basic text input -->

                                <!-- Basic text input -->
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.email')}} <span
                                                class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input type="text" name="email" class="form-control" required="required"
                                               placeholder="{{trans('admin.email')}}" aria-required="true" value="{{ isset($user) ? $user->email  : old('email') }}">
                                        @include('layouts.error', ['input' => 'email'])
                                    </div>
                                </div>
                                <!-- /basic text input -->

                                <div class="form-group row">
                                    <label for="password" class="col-md-3 col-form-label text-md-right">{{trans('admin.pass')}}</label>

                                    <div class="col-md-9">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-3 col-form-label text-md-right"> {{trans('admin.confirm_pass')}}</label>

                                    <div class="col-md-9">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>


                            <div class="text-left">
                                <button type="submit" class="btn btn-primary"><i
                                            class=" icon-arrow-right7 position-left"></i> {{ isset($user) ? trans('admin.update') : trans('admin.add') }}
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection