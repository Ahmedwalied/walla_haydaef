@extends('admin.adminLayout.App')


@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold"> {{trans('admin.home')}} - </span>{{trans('admin.charities')}}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active"><i class="icon-anchor position-left"></i> {{trans('admin.charities')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area ////////////////-->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <a href="/admin/charities/create" class="btn btn-info btn-lg" role="button">{{trans('admin.add')}}</a>
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{trans('admin.charities')}} </h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table  class="table datatable-save-state" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">{{trans('admin.name')}}</th>
                                    <th class="text-center">تابعه لمؤسسه</th>
                                    <th class="text-center">{{trans('admin.address')}}</th>
                                    <th class="text-center">{{trans('admin.description')}}</th>
                                    <th class="text-center">المبلغ المجمع</th>
                                    <th class="text-center">حاله المشروع</th>
                                    <th class="text-center">{{trans('admin.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Element -->
                                @foreach($charities as $charity)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td class="text-center"> {{ $charity->name }}</td>
                                        <td class="text-center"> {{ $charity->foundation->name }}</td>
                                        <td class="text-center"> {{ $charity->address }}</td>
                                        <td class="text-center"> {{ $charity->description }}</td>
                                        <td class="text-center"> {{ $charity->cash }}</td>
                                        <td class="text-center">@if($charity->status == 1)<span class="label label-success">مفعل</span> @else<span class="label label-danger">غير مفعل</span>@endif</td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>
                                               <ul class="dropdown-menu dropdown-menu-right">
                                                   <li>
                                                       <a href="{{'/admin/charities/'.$charity->id.'/edit' }}" data-id="{{$charity->id}}"
                                                          class="text-primary-600">
                                                           <i class="icon-pencil7"> تعديل</i>
                                                       </a>
                                                   </li>
                                                   <li>
                                                       <a href="{{'/admin/charities/'.$charity->id }}" data-id="{{$charity->id}}"
                                                          class="text-green-600">
                                                           <i class="icon-search4"> التفاصيل</i>
                                                       </a>
                                                   </li>
                                                   <li>
                                                       <a href="{{'/admin/funds/'.$charity->id }}" data-id="{{$charity->id}}"
                                                          class="text-black-600">
                                                           <i class="icon-database"> تبرع</i>
                                                       </a>
                                                   </li>
                                                   <li class="suspended-user-form">
                                                       <form method="POST" action="/admin/charities/{{$charity->id}}" >
                                                           <input type="hidden" name="suspended" value="{{($charity->status == 1) ? 0 : 1 }}">
                                                           {!!  method_field('PATCH') !!}
                                                           {{ csrf_field() }}
                                                           <button type="submit"  data-id="{{$charity->id}}" class="text-warning-600"><i class="icon-stop"></i> {{($charity->status == 1) ? 'إيقاف' : 'تفعيل '}}</button>
                                                       </form>
                                                   </li>
                                               </ul>
                                            </ul>
                                        </td>
                                    </tr>
                                    <!-- Element End -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- Content area ////////////////-->
    <!-- Dashboard content -->

@endsection
