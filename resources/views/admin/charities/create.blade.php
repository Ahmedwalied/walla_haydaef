@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{  trans('admin.add') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/charities"><i class="icon-anchor position-left"></i> {{trans('admin.charities')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ trans('admin.add') }} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('admin.add') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal form-validate-jquery" action="/admin/charities{{ isset($charity) }}" method="post"
                              novalidate="novalidate" enctype="multipart/form-data" >
                            {!! isset($charity) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            {{ csrf_field() }}
                            <fieldset class="content-group">

                                <div class="form-group">
                                    <label for="name">اسم المشروع الخيري</label>
                                    <input type="text" class="form-control" placeholder="الاسم" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="address">العنوان</label>
                                    <input type="text" class="form-control" placeholder="العنوان" id="address" name="address">
                                </div>
                                <div class="form-group">
                                    <label for="description">تفاصيل المشروع</label>
                                    <textarea name="description" id="description" class="form-control"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="foundation_id">تابع لمؤسسه</label>
                                    <select name="foundation_id" class="form-control">
                                        <option>اختار مؤسسه</option>
                                            @foreach ($foundation as $f )
                                                @if(!$f->status == 0)
                                                    <option value="{{ $f->id }}">
                                                        {{ $f->name }}
                                                    </option>
                                                @endif
                                            @endforeach
                                    </select>
                                </div>

                            </fieldset>
                            <div class="text-left">
                                <button type="submit" class="btn btn-primary"><i
                                        class=" icon-arrow-right7 position-left"></i> {{  trans('admin.add') }}
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection
