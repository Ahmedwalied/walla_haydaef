@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{  trans('admin.details') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/charities"><i class="icon-anchor position-left"></i> {{trans('admin.charities')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ trans('admin.details') }} </li>
                <li class="active"><i class="icon-import position-left"></i> {{$charity->name}} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('admin.details') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <h5 class="panel-title mb-10">تبرع</h5>
                        <form class="form-horizontal form-validate-jquery" action="{{url('/admin/funds/')}}" method="post"
                              novalidate="novalidate">
                            {!! isset($fund) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            <input type="hidden" value="{{$charity->id}}" name="charity_id">
                            {{ csrf_field() }}
                            <fieldset class="content-group">
                                <table  class="table table-bordered" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                    <thead>
                                    <tr>
                                        <th class="text-center">{{trans('admin.name')}}/{{trans('admin.cash')}}</th>
                                        <th class="text-center">المدفوع</th>
                                        <th class="text-center">{{trans('admin.action')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <select name="user" id="user">
                                                @foreach($users as $user)
                                                    @if($user->user_type == 0)
                                                        <option value="{{$user->id}}">
                                                            <div name="name"> الاسم :{{$user->name}}</div>
                                                            /
                                                            <div name="cash">المبلغ :{{$user->cash}}</div>
                                                        </option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </td>
                                        <td class="text-center">
                                            <input type="number" id="num2" name="cash" min="0"  required>
                                            <span style="color: red">*</span>
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary"><i
                                                        class=" icon-arrow-right7 position-left"></i> تبرع
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                        </form>
                    </div>
                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection
