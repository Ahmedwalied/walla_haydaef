@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{  trans('admin.update') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/charities"><i class="icon-anchor position-left"></i> {{trans('admin.charities')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ trans('admin.update') }} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('admin.update') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal form-validate-jquery" action="/admin/charities/{{$charity->id}}" method="post"
                              novalidate="novalidate">
                            {!! isset($charity) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            {{ csrf_field() }}
                            <fieldset class="content-group">

                                <div class="form-group">
                                    <label for="name">اسم المشروع الخيري</label>
                                    <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{old('about_us') ?? $charity->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="address">العنوان</label>
                                    <input type="text" class="form-control" placeholder="Address" id="address" name="address" value="{{old('address') ?? $charity->address}}">
                                </div>
                                <div class="form-group">
                                    <label for="description">الوصف</label>
                                    <textarea name="description" id="description" class="form-control">{{old('description') ?? $charity->description}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="foundation_id">تابع لمؤسسه :</label>
                                    <select name="foundation_id" class="form-control">
                                        <option>Select Foundation</option>
                                        @foreach ($foundation as $f )
                                            <option value="{{$f->id}}" {{$f->id == $charity->foundation_id ? 'selected' : ''   }} >
                                                {{$f->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                            </fieldset>
                            <div class="text-left">
                                <button type="submit" class="btn btn-primary"><i
                                        class=" icon-arrow-right7 position-left"></i> {{  trans('admin.update') }}
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection
