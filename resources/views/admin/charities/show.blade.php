@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{  trans('admin.details') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/charities"><i class="icon-anchor position-left"></i> {{trans('admin.charities')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ trans('admin.details') }} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('admin.details') }}</h5>
                        <hr>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="panel-body" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                            <fieldset class="content-group">
                                <div class="form-group">
                                    <label for="name">اسم المشروع : </label>
                                    <span>{{$charity->name}}</span>
                                </div>
                                <div class="form-group">
                                    <label for="address">العنوان : </label>
                                    <span>{{$charity->address}}</span>
                                </div>
                                <div class="form-group">
                                    <label for="cash">المبلغ المتبقي بعد التحويل : </label>
                                    <span>{{$charity->cash}}</span>
                                </div>
                                <div class="form-group">
                                    <label for="description">التفاصيل : </label>
                                    <span>{{$charity->description}}</span>
                                </div>
                                <div class="form-group">
                                    <label for="foundation_id">تابعه لمؤسسه : </label>
                                    <span>{{ $charity->foundation->name }}</span>
                                </div>
                            </fieldset>
                    </div>
            </div>
        </div>
        <!-- /main charts -->
    </div>
    </div>
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('admin.details') }}</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                        <div class="panel-body" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                            <div class="panel-body">
                                <div class="form-group">
                                    <h5 class="panel-title">التحويلات</h5>
                                </div>
                                <table  class=" table table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">اسم العميل</th>
                                            <th class="text-center">المراد دفعه</th>
                                            <th class="text-center">المدفوع</th>
                                            <th class="text-center">المتبقي</th>
                                            <th class="text-center">{{trans('admin.created_at')}}</th>
                                            <th class="text-center">تعديل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($funds as $fund)
                                        <tr>
                                            <td class="text-center">
                                                <div>{{$loop->iteration}}</div>
                                            </td>
                                            <td class="text-center">
                                                <div>{{$fund->user->name}}</div>
                                            </td>
                                            <td class="text-center">
                                                <input type="text" value="{{$fund->to_be_paid}}" class="table-input" disabled>
                                            </td>
                                            <form class="form-horizontal form-validate-jquery" action="{{url('/admin/funds/').'/'.$charity->id}}" method="post"
                                                  novalidate="novalidate">
                                                {!! isset($fund) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                                                <input type="hidden" value="{{$fund->id}}" name="id">
                                                <input type="hidden" value="{{$charity->id}}" name="charity_id">
                                                <input type="hidden" value="{{$fund->user_id}}" name="user_id">
                                                <td class="text-center">
                                                    <input type="text" value="{{$fund->cash}}" name="cash" class="table-input">
                                                </td>
                                                @if($fund->to_be_paid > $fund->cash)
                                                    <td class="text-center">
                                                        <input type="text" value="{{$fund->to_be_paid - $fund->cash}}" class="table-input" disabled>
                                                    </td>
                                                @else
                                                    <td class="text-center">
                                                        <input type="text" value="0" class="table-input" disabled>
                                                    </td>
                                                @endif
                                                <td class="text-center">
                                                    <div>
                                                        {{$fund->created_at->diffForHumans()}}
                                                    </div>
                                                </td>
                                                <td>
                                                    @CSRF
                                                    {{--                                                <input type="hidden" name="user_id" value="{{$fund->user_id}}">--}}
                                                    <div class="text-left">
                                                        <button class="btn btn-primary submit"><i
                                                                class=" icon-arrow-right7 position-left"></i> تعديل
                                                        </button>
                                                    </div>
                                                </td>
                                            </form>
                                        </tr>
                                    @endforeach
                                        <tr>
                                            <td>
                                                #
                                            </td>
                                            <td class="text-center">
                                                <span class="text-bold"> المجموع </span>
                                            </td>
                                            <td></td>
                                            @if(!$sum2 == 0)
                                                <td>
                                                    <input value="{{$sum2}}" class="table-input" disabled>
                                                </td>
                                            @else
                                                <td>
                                                    <input disabled>
                                                </td>
                                            @endif
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">

                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                        <div class="panel-body">
                            <div class="form-group">
                                <h5 class="panel-title">التحويلات</h5>
                            </div>
                            <table class="table">
                                <form class="form-horizontal form-validate-jquery" action="{{url('/admin/transfers/')}}" method="post"
                                      novalidate="novalidate">
                                    {{--                                {!! isset($transfer) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}--}}
                                    @CSRF
                                    <input type="hidden" value="{{$charity->cash}}" name="charity_cash">
                                    <input type="hidden" value="{{$charity->foundation_id}}" name="foundation_id">
                                    <input type="hidden" value="{{$charity->id}}" name="charity_id">
                                    <thead>
                                    <tr>
                                        <th class="text-center"><span>المبلغ المجمع  </span></th>
                                        <th class="text-center"><span>المبلغ المحول  </span></th>
                                        <th class="text-center"><span>المبلغ المتبقي في المشروع  </span></th>
                                        <th class="text-center"><span>نحويل </span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <input class="form-control" type="number" id="num1" name="charity_cash" value="{{$charity->cash}}" disabled>

                                        </td>
                                        <td class="text-center">

                                            <input  class="form-control" type="number" id="num2" name="cash" placeholder="تحويل" required>
                                        </td>
                                        <td class="text-center">

                                            <input  class="form-control" type="number" id="remaining" name="remaining" max="{{$charity->cash}}" placeholder="المتبقي" disabled>
                                        </td>
                                        <td>
                                            <div class="text-left">
                                                <button type="submit" class="btn btn-primary"><i
                                                        class=" icon-arrow-right7 position-left"></i> حول
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </form>
                            </table>
                            <hr>
                            <div class="form-group">
                                <h5 class="panel-title">تفاصيل التحويلات</h5>
                            </div>
                            <table  class=" table table-bordered">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">حول</th>
                                    <th class="text-center">الي مؤسسه</th>
                                    <th class="text-center">{{trans('admin.created_at')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($transfers as $transfer)
                                    <tr>
                                        <td class="text-center">
                                            <div>{{$loop->iteration}}</div>
                                        </td>
                                        <td class="text-center">
                                            <div>{{$transfer->cash}}</div>
                                        </td>
                                        <td class="text-center">
                                            <div>{{$transfer->foundation->name}}</div>
                                        </td>
                                        <td class="text-center">
                                            <div>
                                                {{$transfer->created_at->diffForHumans()}}

                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="text-center"><strong>المجموع : </strong></td>
                                    @if(!$sum == 0)
                                        <td class="text-center">{{$sum}}</td>
                                    @else
                                        <td></td>
                                    @endif
                                </tr>
                                </tbody>
                            </table>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
