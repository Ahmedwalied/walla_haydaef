@extends('admin.adminLayout.App')
@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{ isset($contactUs) ? trans('admin.update') : trans('admin.add') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/contact-us"><i class="icon-cash position-left"></i> {{trans('admin.contact-us')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ isset($contactUs) ? trans('admin.update') : trans('admin.add') }} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ isset($contactUs) ? trans('admin.update') : trans('admin.add') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal form-validate-jquery" action="/admin/contact-us{{ isset($contactUs) ? '/'. $contactUs->id : '' }}" method="post"
                              novalidate="novalidate"  >
                            {!! isset($contactUs) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            {{ csrf_field() }}
                            <fieldset class="content-group">

                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.name')}} </label>
                                    <div class="col-lg-9">
                                        <input type="text" name="name" class="form-control" disabled
                                               placeholder="{{trans('admin.name')}}" aria-required="true" value="{{ $contactUs->name }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.email')}} </label>
                                    <div class="col-lg-9">
                                        <input type="email" name="email" class="form-control" disabled
                                               placeholder="{{trans('admin.email')}}" aria-required="true" value="{{ $contactUs->email }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.contact_type')}} </label>
                                    <div class="col-lg-9">
                                        <input type="text" name="contact_type" class="form-control" disabled
                                               placeholder="{{trans('admin.contact_type')}}" aria-required="true" value="@if($contactUs->contact_type == 1) إستفسار @else  شكوى @endif">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.message')}} </label>
                                    <div class="col-lg-9">
                                        <textarea name="message" id="message" cols="30" rows="10" disabled class="form-control">{{$contactUs->message}}</textarea>
                                    </div>
                                </div>

                            </fieldset>
                            {{--<div class="text-left">--}}
                                {{--<button type="submit" class="btn btn-primary"><i--}}
                                            {{--class=" icon-arrow-right7 position-left"></i> {{ isset($contactUs) ? trans('admin.update') : trans('admin.add') }}--}}
                                {{--</button>--}}
                            {{--</div>--}}
                        </form>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection