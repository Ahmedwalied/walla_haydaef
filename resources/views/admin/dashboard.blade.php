@extends('admin.adminLayout.App')
@inject('foundations','App\Models\Foundation')
@inject('charities','App\Models\Charity')
@section('title')الصفحة الرئيسية @endsection
@section('content')
    <div class="row">
    <div class="col-lg-3">
        <div class="all panel bg-indigo-400">
            <ul class="list-inline text-center">
                <li>
                    <i class="icon-users"></i>
                </li>
                <li class="text-center">
                    <div class="text-semibold">{{trans('admin.users')}}</div>
                    <div class="text-muted">{{$users}}</div>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="all panel bg-pink-400">
            <ul class="list-inline text-center">
                <li>
                    <i class="fa fa-users"></i>
                </li>
                <li class="text-center">
                    <div class="text-semibold">الأعضاء الجدد</div>
                    <div class="text-muted">{{$latestUsers}}</div>
                </li>
            </ul>
        </div>
    </div>
        <div class="col-lg-3">
            <div class="all panel bg-blue-400">
                <ul class="list-inline text-center">
                    <li>
                        <i class="icon-anchor"></i>
                    </li>
                    <li class="text-center">
                        <div class="text-semibold">{{trans('admin.foundations')}}</div>
                        <div class="text-muted">{{$foundations->count()}}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="all panel bg-blue-400">
                <ul class="list-inline text-center">
                    <li>
                        <i class="icon-anchor"></i>
                    </li>
                    <li class="text-center">
                        <div class="text-semibold">{{trans('admin.charities')}}</div>
                        <div class="text-muted">{{$charities->count()}}</div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="all panel bg-brown-400">
                <ul class="list-inline text-center">
                    <li>
                        <i class="icon-phone"></i>
                    </li>
                    <li class="text-center">
                        <div class="text-semibold">{{trans('admin.contact-us')}}</div>
                        <div class="text-muted">{{$messages}}</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-8">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">روابط سريعة</h6>
                    <div class="heading-elements">
                        <button type="button" class="btn btn-link heading-btn text-semibold">
                            <i class="icon-calendar3 position-left"></i> <span>{{ date('Y-m-d H:i:s') }}</span>
                        </button>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

                <div class="table-responsive">
                    <table class="table table-xlg text-nowrap">
                        <tbody>
                        <tr>
                            <td class="col-md-8">
                                <div class="media-left media-middle">
                                    <div id="tickets-status"><svg width="42" height="42"><g transform="translate(21,21)"><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(41, 182, 246);" d="M1.1634144591899855e-15,19A19,19 0 0,1 -12.326087772183463,-14.459168725498339L-6.163043886091732,-7.229584362749169A9.5,9.5 0 0,0 5.817072295949927e-16,9.5Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(102, 187, 106);" d="M-12.326087772183463,-14.459168725498339A19,19 0 0,1 14.331188229058796,-12.474656065130077L7.165594114529398,-6.237328032565038A9.5,9.5 0 0,0 -6.163043886091732,-7.229584362749169Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(239, 83, 80);" d="M14.331188229058796,-12.474656065130077A19,19 0 0,1 5.817072295949928e-15,19L2.908536147974964e-15,9.5A9.5,9.5 0 0,0 7.165594114529398,-6.237328032565038Z"></path></g></g></svg></div>
                                </div>
                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">{{trans('admin.users')}} </h5>
                                    <span class="text-muted"><span class="status-mark border-success position-left"></span>{{trans('admin.users')}}</span>
                                </div>
                            </td>
                            <td class="text-right col-md-4">
                                    <a href="{{url('/admin/users/')}}" class="btn bg-teal-400"><i class="icon-plus-circle2 position-left"></i> {{trans('admin.users')}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-8">
                                <div class="media-left media-middle">
                                    <div id="tickets-status"><svg width="42" height="42"><g transform="translate(21,21)"><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(41, 182, 246);" d="M1.1634144591899855e-15,19A19,19 0 0,1 -12.326087772183463,-14.459168725498339L-6.163043886091732,-7.229584362749169A9.5,9.5 0 0,0 5.817072295949927e-16,9.5Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(102, 187, 106);" d="M-12.326087772183463,-14.459168725498339A19,19 0 0,1 14.331188229058796,-12.474656065130077L7.165594114529398,-6.237328032565038A9.5,9.5 0 0,0 -6.163043886091732,-7.229584362749169Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(239, 83, 80);" d="M14.331188229058796,-12.474656065130077A19,19 0 0,1 5.817072295949928e-15,19L2.908536147974964e-15,9.5A9.5,9.5 0 0,0 7.165594114529398,-6.237328032565038Z"></path></g></g></svg></div>
                                </div>
                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">{{trans('admin.foundations')}} </h5>
                                    <span class="text-muted"><span class="status-mark border-success position-left"></span> {{trans('admin.foundations')}}</span>
                                </div>
                            </td>
                            <td class="text-right col-md-4">
                                    <a href="{{url('/admin/foundations/')}}" class="btn bg-teal-400"><i class="icon-plus-circle2 position-left"></i> {{trans('admin.foundations')}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-md-8">
                                <div class="media-left media-middle">
                                    <div id="tickets-status"><svg width="42" height="42"><g transform="translate(21,21)"><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(41, 182, 246);" d="M1.1634144591899855e-15,19A19,19 0 0,1 -12.326087772183463,-14.459168725498339L-6.163043886091732,-7.229584362749169A9.5,9.5 0 0,0 5.817072295949927e-16,9.5Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(102, 187, 106);" d="M-12.326087772183463,-14.459168725498339A19,19 0 0,1 14.331188229058796,-12.474656065130077L7.165594114529398,-6.237328032565038A9.5,9.5 0 0,0 -6.163043886091732,-7.229584362749169Z"></path></g><g class="d3-arc" style="stroke: rgb(255, 255, 255); cursor: pointer;"><path style="fill: rgb(239, 83, 80);" d="M14.331188229058796,-12.474656065130077A19,19 0 0,1 5.817072295949928e-15,19L2.908536147974964e-15,9.5A9.5,9.5 0 0,0 7.165594114529398,-6.237328032565038Z"></path></g></g></svg></div>
                                </div>
                                <div class="media-left">
                                    <h5 class="text-semibold no-margin">{{trans('admin.charities')}} </h5>
                                    <span class="text-muted"><span class="status-mark border-success position-left"></span> {{trans('admin.charities')}}</span>
                                </div>
                            </td>
                            <td class="text-right col-md-4">
                                <a href="{{url('/admin/charities/')}}" class="btn bg-teal-400"><i class="icon-plus-circle2 position-left"></i> {{trans('admin.charities')}}</a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h6 class="panel-title">أحدث الإشعارات</h6>
                    <div class="heading-elements">
                        <span class="badge bg-danger-400 heading-text">@if(isset(Auth::user()->unreadNotifications)){{count(Auth::user()->unreadNotifications)}} @else 0 @endif</span>
                    </div>
                    <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>

                <div class="panel-body">
                    <div class="chart content-group-xs" id="bullets"></div>

                    <ul class="media-list">
                        @if($countUnRead > 0)
                            @foreach($unReadNotifications as $note)
                                <li class="media">
                                    <div class="media-left">
                                        <img src="{{asset('/adminpanel/assets/images/placeholder.jpg')}}" class="img-circle img-sm" alt="">
                                    </div>

                                    <div class="media-body">
                                        <a href="{{url('admin/notifications')}}" class="media-heading">
                                            <span class="text-semibold">{{ $note->data['data'] }}</span>
                                            <span class="media-annotation pull-right">{{$note->created_at->diffforhumans()}}</span>
                                        </a>
                                        <span class="media-annotation">{{$note->data['data'] .' '. $note->data['name'] }}</span>
                                    </div>
                                </li>
                            @endforeach
                        @else
                            <li class="media text-center">
                                لا يوجد اشعارات جديدة
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
