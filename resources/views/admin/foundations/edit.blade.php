@extends('admin.adminLayout.App')

@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{  trans('admin.update') }}</h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/foundations"><i class="icon-anchor position-left"></i> {{trans('admin.foundations')}}</a></li>
                <li class="active"><i class="icon-plus2 position-left"></i> {{ trans('admin.update') }} </li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ trans('admin.update') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <form class="form-horizontal form-validate-jquery" action="/admin/foundations/{{$foundation->id}}" method="post"
                              novalidate="novalidate">
                            {!! isset($foundation) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                            {{ csrf_field() }}
                            <fieldset class="content-group">

                                <div class="form-group">
                                    <label for="name">الاسم</label>
                                    <input type="text" class="form-control" placeholder="Name" id="name" name="name" value="{{old('name') ?? $foundation->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="address">العنوان</label>
                                    <input type="text" class="form-control" placeholder="Address" id="address" name="address" value="{{old('address') ?? $foundation->address}}">
                                </div>
                                <div class="form-group">
                                    <label for="manager_name">اسم صاحب المؤسسه</label>
                                    <input type="text" class="form-control" placeholder="Manager Name" id="manager_name" name="manager_name" value="{{old('manager_name') ?? $foundation->manager_name}}">
                                </div>
                                <div class="form-group">
                                    <label for="about_us">نبذه عن الشركه </label>
                                    <textarea name="about_us" id="about_us" class="form-control">{{old('about_us') ?? $foundation->about_us}}</textarea>
                                </div>
{{--                                <hr>--}}
{{--                                <div class="m-5">--}}
{{--                                    <h3>فروع الشركه</h3>--}}
{{--                                </div>--}}
{{--                                @foreach($branches as $branch)--}}
{{--                                    <div class="form-group">--}}
{{--                                        <h5>({{$loop->iteration}})</h5>--}}
{{--                                        <label for="name">اسم الفرع</label>--}}
{{--                                        <input type="text" name="name" id="name" class="form-control" value="{{old('name') ?? $branch->name}}">--}}
{{--                                    </div>--}}
{{--                                    <div class="form-group">--}}
{{--                                        <label for="address">العنوان</label>--}}
{{--                                        <input type="text" name="address" id="address" class="form-control" value="{{old('address') ?? $branch->address}}">--}}
{{--                                    </div>--}}
{{--                                    <hr>--}}
{{--                                @endforeach--}}
                            </fieldset>
                            <div class="text-left">
                                <button type="submit" class="btn btn-primary"><i
                                        class=" icon-arrow-right7 position-left"></i> {{  trans('admin.update') }}
                                </button>
                            </div>
                        </form>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>

    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection
