@extends('admin.adminLayout.App')


@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold"> {{trans('admin.home')}} - </span>{{trans('admin.foundations')}}
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active"><i class="icon-anchor position-left"></i> {{trans('admin.foundations')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area ////////////////-->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <a href="/admin/foundations/create" class="btn btn-info btn-lg" role="button">{{trans('admin.add')}}</a>
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{trans('admin.foundations')}} </h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table  class="table datatable-save-state" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">{{trans('admin.name')}}</th>
                                    <th class="text-center">{{trans('admin.address')}}</th>
                                    <th class="text-center">{{trans('admin.foundation_status')}}</th>
                                    <th class="text-center">{{trans('admin.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Element -->
                                @foreach($foundations as $foundation)
                                    <tr>
                                        <td class="text-center">{{ $loop->iteration }}</td>
                                        <td class="text-center"> <a href="{{'/admin/foundations/'.$foundation->id }}">{{ $foundation->name }}</a></td>
                                        <td class="text-center"> {{ $foundation->address }}</td>
{{--                                        <td class="text-center"> <img src="/images/categories/{{$foundation->address}}" height="50px" ></td>--}}

                                        <td class="text-center">@if($foundation->status == 1)<span class="label label-success">مفعل</span> @else<span class="label label-danger">غير مفعل</span>@endif</td>
                                        </td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        @can('users update')
                                                            <li><a href="{{url('admin/foundations/'.$foundation->id.'/edit ')}}"
                                                                   data-id="{{$foundation->id}}" class="text-primary-600">
                                                                    <i class="icon-pencil7"></i> تعديل</a></li>
                                                        @endcan
                                                        @can('users read')
                                                                <li><a href="{{'/admin/foundations/'.$foundation->id }}"
                                                                       data-id="{{$foundation->id}}"  class="text-success-600">
                                                                        <i class="icon-search4"></i> التفاصيل</a></li>
                                                        @endcan
                                                        @can('users delete')
                                                            <li class="suspended-user-form">
                                                                <form method="POST" action="/admin/foundations/{{$foundation->id}}" >
                                                                    <input type="hidden" name="suspended" value="{{($foundation->status == 1) ? 0 : 1 }}">
                                                                    {!!  method_field('PATCH') !!}
                                                                    {{ csrf_field() }}
                                                                    <button type="submit"  data-id="{{$foundation->id}}" class="text-warning-600"><i class="icon-stop"></i> {{($foundation->status == 1) ? 'إيقاف' : 'تفعيل '}}</button>
                                                                </form>
                                                            </li>
                                                        @endcan
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <!-- Element End -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- Content area ////////////////-->
    <!-- Dashboard content -->

@endsection
