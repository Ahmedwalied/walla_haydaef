@extends('admin.adminLayout.App')


@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold"> {{trans('admin.details')}} </span>
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active"><i class="icon-anchor position-left"></i> {{trans('admin.details')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area ////////////////-->
    <div class="content">
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{trans('admin.details')}} </h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="table-responsive">
                            <table  class="table" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">{{trans('admin.name')}}</th>
                                    <th class="text-center">{{trans('admin.branches')}}</th>
                                    <th class="text-center">{{trans('admin.address')}}</th>
                                    <th class="text-center">نبذه عن الشركه</th>
                                    <th class="text-center">{{trans('admin.manager')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Element -->
                                    <tr>
                                        <td class="text-center">#</td>
                                        <td class="text-center"> {{ $foundation->name }}</td>
                                        <td class="text-center">
                                            @if(count($foundation->branches) != 0)
                                            @foreach($foundation->branches as $branch)
                                            <ul>
                                                <li>
                                                    <span>{{trans('admin.name')}} : </span>
                                                    <span>{{$branch->name}}</span>
                                                </li>
                                                <li>
                                                    <span>{{trans('admin.address')}} : </span>
                                                    <span>{{$branch->address}}</span>
                                                </li>
                                            </ul>

                                            @endforeach
                                                @else
                                                {{trans('admin.not_found')}}
                                            @endif
                                        </td>
                                        <td> {{ $foundation->address }}</td>
                                        <td> {{ $foundation->about_us }}</td>
                                        <td class="text-center"> {{ $foundation->manager_name }}</td>
                                        {{--                                        <td class="text-center"> <img src="/images/categories/{{$branch->address}}" height="50px" ></td>--}}

                                    </tr>
                                    <!-- Element End -->

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
    </div>
    <!-- Content area ////////////////-->
    <!-- Dashboard content -->
    <div class="content">
    @include('layouts.message')
    <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">

                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                        <div class="panel-body">
                            <div class="form-group">
                                <h5 class="panel-title">اضافه فرع</h5>
                            </div>
                            <table class="table">
                                <form class="form-horizontal form-validate-jquery" action="{{url('/admin/branches/')}}" method="post"
                                      novalidate="novalidate">
{{--                                    {!! isset($branch) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}--}}
                                    @CSRF
                                    <input type="hidden" value="{{$foundation->id}}" name="foundation_id">
                                    <thead>
                                    <tr>
                                        <th class="text-center"><span>اسم الفرع  </span></th>
                                        <th class="text-center"><span>عنوان الفرع  </span></th>
                                        <th class="text-center"><span>اضافه </span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <input class="form-control" type="text" id="name" name="name" placeholder="اسم الفرع" required>
                                        </td>
                                        <td class="text-center">

                                            <input  class="form-control" type="text" id="address" name="address" placeholder="عنوان الفرع" required>
                                        </td>
                                        <td class="text-center">
                                            <div class="d-flex justify-content-center">
                                                <button type="submit" class="btn btn-primary"><i
                                                        class=" icon-arrow-right7 position-left"></i> اضافه
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </form>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
