<div id="modal_theme_danger" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">هل تريد رفض الطلب ?</h6>
            </div>
            <form class="form-horizontal form-validate-jquery" method="post"
                  action="/admin/@if(isset($walletBank))wallets/{{ $walletBank->id  }}@elseif(isset($order))orders/{{ $order->id  }} @endif">
                {!! method_field('PATCH') !!}
                {{ csrf_field() }}
                <input type="hidden" name="is_active" value="2">

                <div class="modal-body">
                    <h6 class="text-semibold">أسباب رفض الطلب </h6>
                    <textarea name="refuse_reason" id="refuse_reason" rows="5" class="form-control"></textarea>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ليس الآن</button>
                    <button type="submit" class="btn btn-warning bg-danger">تأكيد الرفض</button>
                </div>
            </form>
        </div>
    </div>
</div>