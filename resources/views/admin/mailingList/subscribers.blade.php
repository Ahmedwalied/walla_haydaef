@extends('admin.adminLayout.App')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{trans('admin.mailing_list_subscribers_contact')}}</span>
                    - </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active"><i class="icon-comment"></i> {{trans('admin.mailing_list_subscribers_contact')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    @include('layouts.message')
        <!-- Summernote editor -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('admin.message_content')}}</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form class="form-horizontal form-validate-jquery" action="/admin/mailingList" method="post"
                  novalidate="novalidate">
                {{ csrf_field() }}
            <div class="panel-body">
                <div class="form-group">
                    <textarea class="summernote" name="content" >content</textarea>
                </div>
                <button type="submit" class="btn btn-primary btn ">{{trans('admin.send')}}
                    <i class="icon-arrow-left13 position-right"></i>
                </button>
            </div>
            </form>
        </div>
        <!-- /summernote editor -->




    </div>
@endsection

@section('scripts')


    <script type="text/javascript" src="{{ asset('adminpanel/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('adminpanel/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('adminpanel/assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('adminpanel/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('adminpanel/assets/js/pages/editor_summernote.js')}}"></script>
    <!-- /theme JS files -->
@stop