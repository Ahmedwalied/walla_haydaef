@extends('admin.adminLayout.App')

@section('title')
    قائمة الإشعارات
@endsection
@section('styles')

@endsection
@section('scripts')

@endsection
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة الإشعارات</h5>
        </div>

        <div class="panel-body">قائمة تضم جميع الإشعارات الموجودة بالموقع</div>

        <table class="table datatable-save-state">
            <thead>
            <tr>
                <th>#</th>
                <th>الإشعار</th>
                <th>التفاصيل</th>
                <th>التاريخ</th>
                <th>منذ</th>
                <th>الحالة</th>
            </tr>
            </thead>
            <tbody>
            @foreach($notifies as $key=>$one)
                <tr>
                    <td>{{ $key+1 }}</td>
                    <td>{{ $one->data['data'] .' '. $one->data['name'] }}</td>
                    <td>{{isset($one->data['reason']) ? $one->data['reason'] : '-' }}</td>
                    <td class="text-nowrap">{{ $one->created_at->format('Y-m-d')  }}</td>
                    <td>{{ $one->created_at->diffforhumans() }}</td>
                    <td>@if(isset($one->read_at) ) <span class="label label-success">مقروء</span> @else<span class="label label-primary">جديد</span>@endif{{$one->markAsRead()}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <hr>
        <div class="text-center">
            {{ $notifies->links() }}
        </div>
        <br>
    </div>
@endsection
