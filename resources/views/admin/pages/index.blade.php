@extends('admin.adminLayout.App')

@section('title')
    قائمة كل الصفحات الثابتة
@endsection
@section('styles')

@endsection
@section('scripts')

@endsection
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة كل الصفحات الثابتة</h5>
        </div>

        <div class="panel-body">قائمة تضم كل الصفحات الثابتة بالموقع</div>

        <table class="table datatable-save-state">
            <thead>
            <tr>
                <th>#</th>
                <th>العنوان</th>
                <th>التاريخ</th>
                <th>التحديث</th>
                <th>الحالة</th>
                <th class="text-center">الإجراء المتخذ</th>
            </tr>
            </thead>
            <tbody>
            @if(count($pages)>0)
                @foreach($pages as $key=>$one)
                    <tr>
                        {{--@dd(Helpers::app_languages()[0]['code'])--}}
                        <td class="text-primary-600">{{$key+1}}</td>
                        <td>{{$one->title}}</td>
                        <td>{{$one->created_at}}</td>
                        <td>{{$one->updated_at}}</td>
                        <td>@if($one->is_active == 1)<span class="label label-success">مفعل</span> @else<span class="label label-danger">غير مفعل</span>@endif</td>
                        <td class="text-center">
                            <ul class="icons-list">
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon-menu9"></i>
                                    </a>

                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="{{url('admin/pages/'.$one->id.'/edit ')}}" data-id="{{$one->id}}" class="text-primary-600"><i class="icon-pencil7"></i> تعديل</a></li>
                                        <li class="suspended-user-form">
                                            <form method="POST" action="/admin/pages/{{$one->id}}" >
                                                {!!  method_field('DELETE') !!}
                                                {{ csrf_field() }}
                                                <button type="submit"  data-id="{{$one->id}}" class="text-danger-600"><i class="icon-trash"></i> حذف</button>
                                            </form>
                                        </li>
                                        <li class="suspended-user-form">
                                            <form method="POST" action="/admin/pages/{{$one->id}}" >
                                                <input type="hidden" name="suspended" value="{{($one->is_active == 1) ? 0 : 1 }}">
                                                {!!  method_field('PATCH') !!}
                                                {{ csrf_field() }}
                                                <button type="submit"  data-id="{{$one->id}}" class="text-warning-600"><i class="icon-stop"></i> {{($one->is_active == 1) ? 'إيقاف' : 'تفعيل '}}</button>
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            @else لايوجد صفحات ثابتة للعرض @endif
            </tbody>
        </table>
        <a href="/admin/pages/create">
            <button type="button" name="button" style="margin: 20px;" class="btn btn-success pull-right">اضافة صفحة <i class="icon-arrow-left13 position-right"></i></button>
        </a>
    </div>
@endsection
