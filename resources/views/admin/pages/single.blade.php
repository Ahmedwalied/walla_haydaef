@extends('admin.adminLayout.App')
@section('title')
    {{ isset($page) ? 'تعديل' : 'إضافة' }} صفحة
@endsection
@section('styles')
    <link rel="stylesheet" href="{{url('adminpanel/assets/css/switch-hai.css')}}">
@endsection
@section('scripts')
    <!-- summernote plugin -->
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/editors/summernote/editor_summernote.js')}}"></script>
    {{-- <script type="text/javascript" src="/js/load_cities.js"></script> --}}
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/form_inputs.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/ckeditor/ckeditor.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/editor_ckeditor.js')}}"></script>

@endsection

@section('content')
    <form class="form-horizontal form-validate-jquery" method="post"
          action="/admin/pages{{ isset($page) ? '/'.$page->id : '' }}" enctype="multipart/form-data" novalidate="novalidate"  >
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-12">
                <div class="tabbable">

                    <div class="tab-content">
                                <br>
                                <fieldset class="content-group">
                                    <!-- Basic text input -->
                                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                        <div class="col-lg-12">
                                            <input type="text" name="title" class="form-control" required
                                                   placeholder="ضع عنوان هنا" autofocus
                                                   value="{{ isset($page) ? $page->title : old('title')}}">
                                            @if($errors->has('title'))
                                                <span class="help-block">
                                           <strong>{{ $errors->first('title') }}</strong>
                             </span>
                                            @endif
                                        </div>
                                    </div>

                                </fieldset>

                                <div class="panel panel-white border-top-info">
                                    <div class="panel-heading">
                                        <h6 class="panel-title">المحتوى</h6>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="panel-body">
                                        {!! isset($page) ? method_field('PATCH') : '' !!}
                                        {{ csrf_field() }}
                                        <fieldset class="content-group{{ $errors->has('content') ? ' has-error' : '' }}">
                                            <!-- Basic text input -->
                                            <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                                                <div class="col-lg-12">
                                                    <textarea name="content" dir="rtl" rows="10" cols="10" class=" form-control">{{ isset($page) ? $page->content : old('content')}}</textarea>
                                                    @if($errors->has('content'))
                                                        <div class="help-block" >
                                                            <strong>{{  $errors->first('content') }}</strong>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="panel panel-white border-top-info">
                    <div class="panel-heading">
                        <h6 class="panel-title">حالة النشر</h6>
                        <div class="heading-elements">
                            <div class="heading-form" >
                                <strong >التفعيل</strong>
                                <label class="switch">
                                    <input type="checkbox" name="is_active" {{isset($page) ? ($page->is_active==1) ? 'checked' : '' : 'checked'}} value="{{isset($page) ? ($page->is_active==1) ? 1 : 0 : 1}}">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="panel-body">
                        <fieldset class="content-group">
                            <div class="form-group">
                                <p><i class="fa fa-key"></i> <strong>الحالة : </strong> {{ isset($page) ? ($page->is_active==1)? 'منشور' : 'إيقاف' : 'جديد'}}</p>
                                <p><i class="fa fa-calendar"></i> <strong>الإنشاء : </strong> {{ isset($page) ? $page->created_at : 'لا يوجد'}}</p>
                                <p><i class="fa fa-calendar-o"></i> <strong>التحديث : </strong> {{ isset($page) ? $page->updated_at : 'لا يوجد'}}</p>
                            </div>
                            <div class="row">
                                <button type="reset" class="btn btn-default col-sm-6" id="reset">
                                    <i class="icon-reload-alt position-left"></i>استعادة
                                </button>
                                <button type="submit" class="btn btn-primary col-sm-6">{{ isset($page) ? 'تعديل' : 'إضافة' }}
                                    <i class=" icon-arrow-left13 position-right"></i>
                                </button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <!-- Form validation -->


@endsection
