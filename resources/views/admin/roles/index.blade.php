@extends('admin.adminLayout.App')

@section('title')
    قائمة كل الأدوار
@endsection
@section('styles')
    <link rel="stylesheet" href="{{url('adminpanel/assets/css/switch-hai.css')}}">
@endsection
@section('scripts')

@endsection
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة كل الأدوار</h5>
        </div>

        <div class="panel-body">قائمة تضم كل الأدوار بالموقع</div>

        <table class="table datatable-save-state">
            <thead>
            <tr>
                <th>#</th>
                <th>الدور</th>
                <th class="text-center">الإجراء المتخذ</th>
            </tr>
            </thead>
            <tbody>
            @if(count($roles)>0)
            @foreach($roles as $key=>$one)
            <tr>
                <td class="text-primary-600">{{$key+1}}</td>
                <td>{{$one->name}}</td>
                <td class="text-center">
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>

                            <ul class="dropdown-menu dropdown-menu-right">
                                <li><a href="{{url('admin/roles/'.$one->id.'/edit ')}}" data-id="{{$one->id}}" class="text-primary-600"><i class="icon-pencil7"></i> تعديل</a></li>
                                <li class="suspended-user-form">
                                    <form method="POST" action="/admin/roles/{{$one->id}}" >
                                        {!!  method_field('DELETE') !!}
                                        {{ csrf_field() }}
                                        <button type="submit"  data-id="{{$one->id}}" class="text-danger-600"><i class="icon-trash"></i> حذف</button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
            @endforeach
            @else لايوجد أدوار للعرض @endif
            </tbody>
        </table>
        <a href="/admin/roles/create">
            <button type="button" name="button" style="margin: 20px;" class="btn btn-success pull-right">اضافة دور <i class="icon-arrow-left13 position-right"></i></button>
        </a>
    </div>
@endsection
