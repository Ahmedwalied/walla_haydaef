@extends('admin.adminLayout.App')
@section('title')
    {{ isset($role) ? 'تعديل' : 'إضافة' }} دور
@endsection
@section('styles')
    <link rel="stylesheet" href="{{url('adminpanel/assets/css/switch-hai.css')}}">
@endsection
@section('scripts')

@endsection
@section('content')
    <form class="form-horizontal form-validate-jquery" method="post"
          action="/admin/roles{{ isset($role) ? '/'.$role->id : '' }}" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-white border-top-info">
                <div class="panel-heading">
                    <h6 class="panel-title">{{ isset($role) ? 'تعديل' : 'إضافة' }} دور</h6>

                </div>

                <div class="panel-body">
                        {!! isset($role) ? method_field('PATCH') : '' !!}
                        {{ csrf_field() }}
                        <fieldset class="content-group">
                            <!-- Basic text input -->
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="control-label col-lg-3">اسم الدور <span class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" name="name" class="form-control" required
                                           placeholder="ضع إسم الدور هنا" autofocus
                                           value="{{ isset($role) ? $role->name : old('name')}}">
                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                           <strong>{{ $errors->first('name') }}</strong>
                                       </span>
                                    @endif
                                </div>
                            </div>


                    </fieldset>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="panel panel-white border-top-info">
                <div class="panel-heading">
                    <h6 class="panel-title">الصلاحيات</h6>
                </div>
            </div>

            @if (count($tables)>0)
            <div class="row">
                @foreach ($tables as $key =>$table)
                    <div class="col-md-4">
                        <div class="panel panel-white border-top-info">
                            <div class="panel-heading">
                                <h6 class="panel-title">{{$table->title_ar}}</h6>
                            </div>
                            <div class="panel-body">
                                <fieldset class="content-group">
                                @php($id=(($key+1)*count($perms))-3)
                                @for($i =0 ;$i <count($perms);$i++)
                                    <div class="checkbox-hai col-xs-3">
                                    <label for="{{$table->title .'-'.$perms[$i]}}">
                                        <input type="checkbox" class="control-info"
                                               @if(isset($role)) @if($role->hasPermissionTo($i + $id)) checked @endif @endif
                                               id="{{$table->title .'-'.$perms[$i]}}" name="{{$table->title .'-'.$perms[$i]}}" value="{{$i + $id}}">
                                        <span>{{$perms[$i]}}</span>
                                    </label>
                                </div>
                                @endfor
                                </fieldset>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            @else
                <p>No Permissions Available</p>
            @endif
                </div>
            </div>
        </div>
    <div class="text-left">
        <button type="submit" class="btn btn-primary">
            <i class=" icon-arrow-right7 position-left"></i> {{ isset($role) ? 'تعديل' : 'إضافة' }}
        </button>
        <button type="reset" class="btn btn-default" id="reset">استعادة
            <i class="icon-reload-alt position-right"></i>
        </button>
    </div>
    </form>

    <!-- Form validation -->


@endsection