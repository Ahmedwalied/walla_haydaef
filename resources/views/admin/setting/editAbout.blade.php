@extends('admin.adminLayout.App')

@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{trans('admin.about_us_edit')}}</span>
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active">{{trans('admin.about_us_edit')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    @include('layouts.message')
        <!-- Summernote editor -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('admin.content')}}</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <form class="form-horizontal form-validate-jquery" action="/admin/about/{{$about->id}}" method="post" novalidate="novalidate" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="container">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#general">{{trans('admin.general')}}</a></li>
                        <li><a data-toggle="tab" href="#arabic">{{trans('locale.ar')}}</a></li>
                        <li><a data-toggle="tab" href="#english">{{trans('locale.en')}}</a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="general" class="tab-pane fade in active">
                            <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label text-semibold">{{trans('admin.video')}}</label>
                                <div class="col-lg-9">
                                    <input type="file" class="file-input" data-browse-class="btn btn-primary btn-block"
                                           data-show-remove="false" data-show-caption="false" data-show-upload="false"
                                           value="{{$about->video}}" name="video">
                                    @include('layouts.error', ['input' => 'video'])
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('video') ? ' has-error' : '' }}">
                                <label class="col-lg-3 control-label text-semibold"></label>
                                <div class="col-lg-9">
                                    @if(isset($about->video))
                                        <video id="my-video" class="max-width video-js" data-setup="{}" controls="controls"
                                               src="/videos/{{$about->video }}"></video>
                                    @endif
                                </div>
                            </div>
                        <!-- End Edite -->
                        </div>
                        <div id="arabic" class="tab-pane fade">

                            <!-- Basic text input -->
                            <div class="form-group">
                                <label class="control-label col-lg-3">{{trans('admin.title')}} <span
                                            class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" name="ar_title" class="form-control" required="required"
                                           placeholder="{{trans('admin.title')}}  " aria-required="true" value="{{$about->translation('ar')->first()->title}} ">
                                    @include('layouts.error', ['input' => 'ar_title'])
                                </div>
                            </div>
                            <!-- /basic text input -->

                            <div class="panel-body">
                                <div class="form-group">
                                    <textarea class="summernote" name="ar_description">{{$about->translation('ar')->first()->description}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div id="english" class="tab-pane fade">
                            <!-- Basic text input -->
                            <div class="form-group">
                                <label class="control-label col-lg-3">{{trans('admin.title')}} <span
                                            class="text-danger">*</span></label>
                                <div class="col-lg-9">
                                    <input type="text" name="en_title" class="form-control" required="required"
                                           placeholder="{{trans('admin.title')}} " aria-required="true" value="{{$about->translation('en')->first()->title}} ">
                                    @include('layouts.error', ['input' => 'en_title'])
                                </div>
                            </div>
                            <!-- /basic text input -->
                            <div class="panel-body">
                                <div class="form-group">
                                    <textarea class="summernote" name="en_description">{{$about->translation('en')->first()->description}}</textarea>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="text-left">
                        <button type="submit" class="btn bg-teal-400">{{trans('admin.update')}}<i class="icon-arrow-left13 position-right"></i></button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /summernote editor -->




    </div>
    <!-- /content area -->

@endsection

@section('js_scripts')


    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/pages/editor_summernote.js')}}"></script>
    <!-- /theme JS files -->
@stop