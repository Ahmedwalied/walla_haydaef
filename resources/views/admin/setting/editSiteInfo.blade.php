@extends('admin.adminLayout.App')
@section('content')

    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold">{{trans('admin.edit_site_info')}}</span>
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li class="active">{{trans('admin.edit_site_info')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->
    <div class="content">
    @include('layouts.message')
        <!-- Summernote editor -->
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">{{trans('admin.site_info')}} </h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <li><a data-action="reload"></a></li>
                        <li><a data-action="close"></a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body">
            <form class="form-horizontal form-validate-jquery" action="/admin/info/{{$info->id}}" method="post" novalidate="novalidate" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3">{{trans('admin.site_name')}} <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="site_name" class="form-control" required="required"
                               placeholder="{{trans('admin.site_name')}} " aria-required="true" value="{{$info->site_name}} ">
                        @include('layouts.error', ['input' => 'site_name'])
                    </div>
                </div>
                <!-- /basic text input -->


                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3">{{trans('admin.site_description')}} <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <textarea type="text" name="site_description" class="form-control" required="required"
                               placeholder="{{trans('admin.site_logo')}} " aria-required="true">
                            {{$info->site_description}}
                        </textarea>
                        @include('layouts.error', ['input' => 'site_description'])
                    </div>
                </div>
                <!-- /basic text input -->


                <!-- /basic text input -->
                <div class="form-group{{ $errors->has('site_front_image') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label">{{trans('admin.site_front_image')}}
                        <span class="text-danger"></span></label>
                    <div class="col-lg-9">
                        <input type="file" class="file-input" data-browse-class="btn btn-primary btn-block"
                               data-show-remove="false" data-show-caption="false" data-show-upload="false"
                               value="" name="site_front_image">
                        @include('layouts.error', ['input' => 'site_front_image'])
                    </div>
                </div>
                <!-- End Edite -->

                @if(isset($info))
                    <input type="hidden" value="{{ $info->site_front_image }}" name="old_image">
                    <!-- /basic text input -->
                    <div class="form-group{{ $errors->has('site_front_image') ? ' has-error' : '' }}">
                        <div class="col-lg-3"></div>
                        <div class="col-lg-9">
                            <img src="/images/{{$info->site_front_image}}" class="img-responsive center-block logo_admin">
                        </div>
                    </div>
                @endif
            <!-- End Edite -->


                    <h4 class="text-center">{{trans('admin.contact_methods')}} </h4>
                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3">{{trans('admin.tel')}}  <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="telephone" class="form-control" required="required"
                               placeholder="{{trans('admin.tel')}}  " aria-required="true" value="{{$info->telephone}} ">
                        @include('layouts.error', ['input' => 'telephone'])
                    </div>
                </div>
                <!-- /basic text input -->
                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3">{{trans('admin.email')}} <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="email" class="form-control" required="required"
                               placeholder="{{trans('admin.email')}}  " aria-required="true" value="{{$info->email}} ">
                        @include('layouts.error', ['input' => 'email'])
                    </div>
                </div>
                <!-- /basic text input -->

  
                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3">{{trans('admin.address')}} <span
                                class="text-danger">*</span></label>
                    <div class="col-lg-9">
                        <input type="text" name="address" class="form-control" required="required"
                               placeholder="{{trans('admin.address')}}  " aria-required="true" value="{{$info->address}} ">
                        @include('layouts.error', ['input' => 'address'])
                    </div>
                </div>
                <!-- /basic text input -->
                    <h4 class="text-center">{{trans('admin.social_media')}}</h4>
                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3"><i class="label-icon-lg icon-facebook"></i></label>
                    <div class="col-lg-9">
                        <input type="text" name="facebook" class="form-control" required="required"
                               placeholder="facebook " aria-required="true" value="{{$info->facebook}} ">
                        @include('layouts.error', ['input' => 'facebook'])
                    </div>
                </div>
                <!-- /basic text input -->

                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3"><i class="label-icon-lg icon-twitter"></i></label>
                    <div class="col-lg-9">
                        <input type="text" name="twitter" class="form-control" required="required"
                               placeholder="twitter " aria-required="true" value="{{$info->twitter}} ">
                        @include('layouts.error', ['input' => 'twitter'])
                    </div>
                </div>
                <!-- /basic text input -->

                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3"><i class="label-icon-lg  icon-linkedin"></i></label>
                    <div class="col-lg-9">
                        <input type="text" name="linkedin" class="form-control" required="required"
                               placeholder="linkedin " aria-required="true" value="{{$info->linkedin}} ">
                        @include('layouts.error', ['input' => 'linkedin'])
                    </div>
                </div>
                <!-- /basic text input -->

                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3"><i class="label-icon-lg icon-youtube"></i></label>
                    <div class="col-lg-9">
                        <input type="text" name="youtube" class="form-control" required="required"
                               placeholder="youtube " aria-required="true" value="{{$info->youtube}} ">
                        @include('layouts.error', ['input' => 'youtube'])
                    </div>
                </div>
                <!-- /basic text input -->

                <!-- Basic text input -->
                <div class="form-group">
                    <label class="control-label col-lg-3"><i class="label-icon-lg icon-instagram"></i></label>
                    <div class="col-lg-9">
                        <input type="text" name="instgram" class="form-control" required="required"
                               placeholder="instgram " aria-required="true" value="{{$info->instgram}} ">
                        @include('layouts.error', ['input' => 'instgram'])
                    </div>
                </div>
                <!-- /basic text input -->
                


            
                <div class="text-left">
                    <button type="submit" class="btn bg-teal-400">{{trans('admin.update')}} <i class="icon-arrow-left13 position-right"></i></button>
                </div>
            </div>
            </form>
        </div>
        </div>
        <!-- /summernote editor -->




    </div>
    <!-- /content area -->

@endsection

@section('js_scripts')


    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/editors/summernote/summernote.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/pages/editor_summernote.js')}}"></script>
    <!-- /theme JS files -->
@stop