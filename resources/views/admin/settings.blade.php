@extends('admin.adminLayout.App')

@section('title')
    {{trans('admin.edit_site_info')}}
@endsection

@section('scripts')
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/form_inputs.js')}}"></script>
@endsection
@section('content')
    @include('admin.layout.alert')
    <div class="panel panel-flat border-top-info">
        <div class="panel-heading">
            <h6 class="panel-title">{{trans('admin.edit_site_info')}} </h6>
            <a class="heading-elements-toggle"><i class="icon-menu"></i></a></div>
        <form class="form-horizontal form-validate-jquery" action="/admin/settings/store" method="post"
              novalidate="novalidate" enctype="multipart/form-data">
            {{ csrf_field() }}
        <div class="panel-body">
            <div class="tabbable">
                <ul class="nav nav-tabs bg-slate nav-tabs-component">
                    <li class="active"><a href="#colored-rounded-tab1" data-toggle="tab" aria-expanded="true">{{trans('admin.site_info')}}</a></li>
                    <li class=""><a href="#colored-rounded-tab2" data-toggle="tab" aria-expanded="false">{{trans('admin.contact_methods')}}</a></li>
                    <li class=""><a href="#colored-rounded-tab3" data-toggle="tab" aria-expanded="false"> {{trans('admin.social_media')}}</a></li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane active" id="colored-rounded-tab1">
                        <br>
                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"> {{trans('admin.site_name')}}<span
                                        class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="text" name="title" class="form-control" required="required"
                                       placeholder="{{trans('admin.site_name')}}" aria-required="true"   value="{{ isset($settings) ? $settings->title : old('title')}}">
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('title') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3">{{trans('admin.site_description')}}</label>
                            <div class="col-lg-9">
                                 <textarea name="description" class="form-control " placeholder="{{trans('admin.site_description')}}" rows="5">{{ isset($settings) ? $settings->description : old('description')}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('description') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                            <label class="col-lg-3 control-label text-semibold">{{trans('admin.site_logo')}}</label>
                            <div class="col-lg-9">
                                @if(isset($settings->logo)) <img src="{{asset($settings->logo)}}" class="dashboard-img">@endif
                                <input type="file" class="file-input file-styled-primary" data-browse-class="btn btn-primary btn-block"
                                        name="logo">
                                @if(isset($settings) && $settings->logo )
                                    <input type="hidden" value="{{$settings->logo}}" name="old-logo">
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('copyrights') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3">{{trans('admin.copyrights')}}</label>
                            <div class="col-lg-9">
                                <input type="text" name="copyrights" class="form-control"
                                       placeholder="{{trans('admin.copyrights')}}"  value="{{ isset($settings) ? $settings->copyrights : old('copyrights')}}">
                                @if ($errors->has('copyrights'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('copyrights') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('work_times') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"> {{trans('admin.work_hours')}}</label>
                            <div class="col-lg-9">
                                <input type="text" name="work_times" class="form-control"
                                       placeholder="{{trans('admin.work_hours')}}" aria-required="true"   value="{{ isset($settings) ? $settings->work_times : old('work_times')}}">
                                @if ($errors->has('work_times'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('work_times') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>

                    </div>

                    <div class="tab-pane" id="colored-rounded-tab2">
                        <br>
                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"> {{trans('admin.address')}}</label>
                            <div class="col-lg-9">
                                <input type="text" name="address" class="form-control"
                                       placeholder="{{trans('admin.address')}}"  value="{{ isset($settings) ? $settings->address : old('address')}}">
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('address') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('whatsapp') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3">Whatsapp</label>
                            <div class="col-lg-9">
                                <input type="url" name="whatsapp" class="form-control"
                                       placeholder="Whatsapp" value="{{ isset($settings) ? $settings->whatsapp : old('whatsapp')}}">
                                @if ($errors->has('whatsapp'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('whatsapp') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3">{{trans('admin.tel')}}</label>
                            <div class="col-lg-9">
                                <input type="text" name="phone" class="form-control"
                                       placeholder="{{trans('admin.tel')}}"  value="{{ isset($settings) ? $settings->phone : old('phone')}}">
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('phone') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"> {{trans('admin.email')}}<span
                                        class="text-danger">*</span></label>
                            <div class="col-lg-9">
                                <input type="email" name="email" class="form-control" required="required"
                                       placeholder="{{trans('admin.email')}}" aria-required="true"   value="{{ isset($settings) ? $settings->email : old('email')}}">
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('email') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('map') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"> {{trans('admin.map')}}</label>
                            <div class="col-lg-9">
                                 <textarea name="map" class="form-control " placeholder="{{trans('admin.map')}}" rows="5">{{ isset($settings) ? $settings->map : old('map')}}</textarea>
                                @if ($errors->has('map'))
                                    <span class="help-block">
                                           <strong>{{ $errors->first('map') }}</strong>
                                       </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="colored-rounded-tab3">
                        <br>
                        <div class="form-group{{ $errors->has('facebook') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"><i class="icon icon-facebook"></i> </label>
                            <div class="col-lg-9">
                                <input type="url" name="facebook" class="form-control"
                                       placeholder=" Facebook URL" value="{{ isset($settings) ? $settings->facebook : old('facebook')}}">
                                @if ($errors->has('facebook'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('facebook') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('linkedin') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"><i class="icon icon-linkedin"></i></label>
                            <div class="col-lg-9">
                                <input type="url" name="linkedin" class="form-control"
                                       placeholder="Linkedin URL" value="{{ isset($settings) ? $settings->linkedin : old('linkedin')}}">
                                @if ($errors->has('linkedin'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('linkedin') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('twitter') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"><i class="icon icon-twitter"></i></label>
                            <div class="col-lg-9">
                                <input type="url" name="twitter" class="form-control"
                                       placeholder=" Twitter URL" value="{{ isset($settings) ? $settings->twitter : old('twitter')}}">
                                @if ($errors->has('twitter'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('twitter') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('snapchat') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"><i class="fa fa-snapchat"></i></label>
                            <div class="col-lg-9">
                                <input type="url" name="snapchat" class="form-control"
                                       placeholder=" snapchat URL" value="{{ isset($settings) ? $settings->snapchat : old('snapchat')}}">
                                @if ($errors->has('snapchat'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('snapchat') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('youtube') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"><i class="icon icon-youtube"></i></label>
                            <div class="col-lg-9">
                                <input type="url" name="youtube" class="form-control"
                                       placeholder=" youtube URL" value="{{ isset($settings) ? $settings->youtube : old('youtube')}}">
                                @if ($errors->has('youtube'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('youtube') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('instagram') ? ' has-error' : '' }}">
                            <label class="control-label col-lg-3"><i class="icon icon-instagram"></i></label>
                            <div class="col-lg-9">
                                <input type="url" name="instagram" class="form-control"
                                       placeholder=" instagram URL" value="{{ isset($settings) ? $settings->instagram : old('instagram')}}">
                                @if ($errors->has('instagram'))
                                    <span class="help-block">
                                               <strong>{{ $errors->first('instagram') }}</strong>
                                           </span>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="text-right">
                <button type="submit" class="btn btn-primary"> تأكيد البيانات <i class="icon-arrow-left13 position-right"></i></button>
            </div>
        </div>
        </form>
    </div>
@endsection
