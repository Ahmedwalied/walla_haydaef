@extends('admin.adminLayout.App')

@section('title')
    قائمة الاعضاء
@endsection
@section('styles')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/tables/datatables/extensions/select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/datatables_extension_buttons_print.js')}}"></script>
@endsection
@section('content')
    @include('layouts.message')

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة الاعضاء</h5>
        </div>

        <div class="panel-body">قائمة تضم جميع الأعضاء المسجلين بالموقع</div>

        <!-- Main charts -->
        <div class="row">
            <div class="col-lg-12">

                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{trans('admin.users')}} </h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <table  class="table datatable-save-state" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr >
                                    <th class="text-center">{{trans('admin.name')}}</th>
                                    <th class="text-center">{{trans('admin.phone')}}</th>
                                    <th class="text-center">{{trans('admin.wallet')}}</th>
                                    <th class="text-center">{{trans('admin.image')}}</th>
                                    <th class="text-center">{{trans('admin.status')}}</th>
                                    <th class="text-center">{{trans('admin.action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- Element -->
                                @foreach($users as $user)
                                    <tr>
                                        <td class="text-center">{{ $user->name }} </td>
                                        <td class="text-center">{{ $user->phone }}</td>
                                        <td class="text-center">{{ $user->cash }}</td>
                                        @if(!$user->image == null)
                                            <td class="text-center"><img src="/images/users/{{$user->image}}" height="50px" ></td>
                                        @else
                                            <td class="text-center"><img src="/images/images.png" height="50px" ></td>
                                        @endif
                                        <td class="text-center">@if($user->is_active == 1)<span class="label label-success">مفعل</span> @else<span class="label label-danger">غير مفعل</span>@endif</td>
                                        <td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        @can('users update')
                                                            <li>
                                                                <a href="{{url('admin/users/'.$user->id.'/edit ')}}"
                                                                   data-id="{{$user->id}}" class="text-primary-600">
                                                                    <i class="icon-pencil7"></i> تعديل</a>
                                                            </li>
                                                        @endcan
                                                        @can('users read')
                                                                <li><a href="{{'/admin/users/'.$user->id }}"  data-id="{{$user->id}}" class="text-green-600"><i class="icon-search4"></i> التفاصيل</a></li>
                                                        @endcan
                                                            @can('users delete')
                                                                <li class="suspended-user-form">
                                                                    <form method="POST" action="/admin/users/{{$user->id}}" >
                                                                        <input type="hidden" name="suspended" value="{{($user->is_active == 1) ? 0 : 1 }}">
                                                                        {!!  method_field('PATCH') !!}
                                                                        {{ csrf_field() }}
                                                                        <button type="submit"  data-id="{{$user->id}}" class="text-warning-600"><i class="icon-stop"></i> {{($user->is_active == 1) ? 'إيقاف' : 'تفعيل '}}</button>
                                                                    </form>
                                                                </li>
                                                            @endcan
                                                    </ul>
                                                </li>
                                            </ul>
                                        </td>

                                    </tr>
                                    <!-- Element End -->
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /bordered panel body table -->
            </div>
        </div>
        <!-- /main charts -->
        <a href="/admin/users/create">
            <button type="button" name="button" style="margin: 20px;" class="btn btn-success pull-right">اضافة عضو <i class="icon-arrow-left13 position-right"></i></button>
        </a>
    </div>
@endsection
