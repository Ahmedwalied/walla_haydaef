<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> تسجيل الدخول لوحة تحكم{{ config('app.name', 'Laravel') }} </title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{url('adminpanel/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('adminpanel/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('adminpanel/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('adminpanel/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('color')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>

    <script type="text/javascript" src="{{url('adminpanel/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/login.js')}}"></script>
    <!-- /theme JS files -->

</head>

<body >

<!-- Page container -->
<div class="page-container login-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Content area -->
            <div class="content">

                <!-- Tabbed form -->
                <div class="tabbable panel login-form ">
{{--                    <ul class="nav nav-tabs nav-justified">--}}
{{--                        <li class="active"><a href="#basic-tab1" data-toggle="tab"><h6><i class="icon-checkmark3 position-left"></i> عضو مسجل سابقاً?</h6></a></li>--}}
{{--                    </ul>--}}

                    <div class="tab-content panel-body">
                        <div class="tab-pane fade in active" id="basic-tab1">
                            <form  method="POST" action="{{ route('admin.login') }}">
                                @csrf
                                <div class="text-center">
{{--                                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>--}}
                                    <img src="/images/logo.png" alt="" style="width:110px;height:110px;margin: 10px;">
                                    <h5 class="content-group">تسجيل الدخول الى حسابك <small class="display-block">سجل بيانات الدخول لحسابك</small></h5>
                                </div>

                                <div class="form-group has-feedback has-feedback-left{{ $errors->has('email') ? ' has-error' : '' }} ">
                                    <input type="email" class="form-control  @error('email') is-invalid @enderror" id="email" placeholder="البريد الاكتروني" name="email" required="required" autocomplete="email"  >
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                    @error('email')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group has-feedback has-feedback-left{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="كلمه المرور">
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                    @error('password')
                                    <span class="help-block" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>

                                <div class="form-group login-options">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="styled" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                تذكرني
                                            </label>
                                        </div>

{{--                                        <div class="col-sm-7 text-right">--}}
{{--                                            @if (Route::has('password.request'))--}}
{{--                                                <a class="btn btn-link" href="{{ route('password.request') }}">--}}
{{--                                                    هل نسيت كلمة المرور ؟--}}
{{--                                                </a>--}}
{{--                                            @endif--}}
{{--                                        </div>--}}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">تسجيل الدخول <i class="icon-arrow-left13 position-right"></i></button>
                                </div>
                            </form>


{{--                            <span class="help-block text-center no-margin">بالاستكمال انت توافق على الشروط و الأحكام الخاصة  <a href="/">الموقع الإلكتروني</a> </span>--}}
                        </div>
                    </div>
                </div>
                <!-- /tabbed form -->


                <!-- Footer -->
                <div class="footer text-white">
                    &copy; {{date('Y')}}. <a href="https://www.ibtdi.com/" class="text-white">لوحة التحكم الموقع من ابتدي لتصميم المواقع</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
</html>
