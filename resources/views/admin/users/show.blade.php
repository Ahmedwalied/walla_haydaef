@extends('admin.adminLayout.App')


@section('content')
    <!-- Page header -->
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i> <span class="text-semibold"> {{trans('admin.details')}} </span>
                </h4>
            </div>
        </div>

        <div class="breadcrumb-line">
            <ul class="breadcrumb">
                <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
                <li><a href="/admin/users"><i class="icon-users position-left"></i> {{trans('admin.users')}}</a></li>
                <li class="active"><i class="icon-anchor position-left"></i> {{trans('admin.details')}}</li>
            </ul>

        </div>
    </div>
    <!-- /page header -->

    <!-- Content area ////////////////-->
    <div class="content">
    @include('layouts.message')
        <div class="row">
                        <div class="col-lg-12">
                            <!-- Bordered panel body table -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h5 class="panel-title">{{ trans('admin.details') }}</h5>
                                    <div class="heading-elements">

                                        <ul class="icons-list">

                                            <li><a data-action="collapse"></a></li>
                                            <li><a data-action="reload"></a></li>
                                            <li><a data-action="close"></a></li>

                                        </ul>
                                    </div>
                                </div>

                                <div class="panel-body" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                    <fieldset class="content-group">
                                        <div class="form-group">
                                            @if(!$user->image == null)
                                                <td class="text-center"><img src="/images/users/{{$user->image}}" height="100px" ></td>
                                            @else
                                                <td class="text-center"><img src="/images/images.png" height="100px" ></td>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="name">الاسم : </label>
                                            <span>{{$user->name}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_name">اسم المستخدم : </label>
                                            <span>{{$user->user_name}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="phone">رقم الهاتف : </label>
                                            <span>{{$user->phone}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_type">نوع العضو : </label>
                                            @if($user->user_type == 0)
                                                <span>عضو عادي</span>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="bio">السيرة الذاتيه : </label>
                                            <span>{{$user->bio}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">البريد الالكتروني : </label>
                                            <span>{{$user->email}}</span>
                                        </div>
                                        <div class="form-group">
                                            <label for="cash">المبلغ : </label>
                                            <span>{{$user->cash}}</span>
                                        </div>
                                        <div class="form-group">@if($user->is_active == 1)<span class="label label-success" style="width: 80px;height: 20px">مفعل</span> @else<span class="label label-danger">غير مفعل</span>@endif</div>
                                    </fieldset>
                                </div>
                            </div>
                            <!-- /bordered panel body table -->
                        </div>
                    </div>

        <!-- /main charts -->
    </div>
    <div class="content">
        @include('layouts.message')
        <div class="row">
            <div class="col-lg-12">
                <!-- Bordered panel body table -->
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">التبرعات</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <table  class="table" dir="{{ app()->getLocale()=='ar' ? 'rtl':'ltr' }}">
                                <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">{{trans('admin.name')}}</th>
                                    <th class="text-center">المراد دفعه</th>
                                    <th class="text-center">المبلغ المدفوع</th>
                                    <th class="text-center">المتبقي</th>
                                    <th class="text-center">التاريخ</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($funds as $fund)
                                    <tr>
                                        <td class="text-center">
                                            <div>{{$loop->iteration}}</div>
                                        </td>
                                        <td class="text-center">
                                            <div>{{$fund->charity->name}}</div>
                                        </td>
                                        <td class="text-center">
                                            <input value="{{$fund->to_be_paid}}"  disabled>
                                        </td>
                                        <td class="text-center">
                                            <input value="{{$fund->cash}}" disabled>
                                        </td>
                                        @if($fund->to_be_paid > $fund->cash)
                                            <td class="text-center">
                                                <input value="{{$fund->to_be_paid - $fund->cash}}" disabled>
                                            </td>
                                            @else
                                            <td class="text-center">
                                                <input value="0" disabled>
                                            </td>
                                        @endif
                                        <td class="text-center">
                                            <div>
                                                {{$fund->created_at->diffForHumans()}}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
