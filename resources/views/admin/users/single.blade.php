@extends('admin.adminLayout.App')
@section('styles')
    <link rel="stylesheet" href="{{url('adminpanel/assets/css/switch-hai.css')}}" xmlns="http://www.w3.org/1999/html">
@endsection
@section('content')
    <!-- Page header -->
    <div class="breadcrumb-line bg-info">
        <ul class="breadcrumb">
            <li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>
            <li><a href="/admin/users"><i class="icon-users position-left"></i> {{trans('admin.users')}}</a></li>
            <li class="active"><i class="icon-plus2 position-left"></i> {{ isset($user) ? trans('admin.update') : trans('admin.add') }} </li>
        </ul>

    </div>
    <div class="page-header">
        <div class="page-header-content">
            <div class="page-title">
                <h4><i class="icon-arrow-right6 position-left"></i>{{ isset($user) ? trans('admin.update') : trans('admin.add') }}</h4>
            </div>
        </div>

        {{--<div class="breadcrumb-line">--}}
            {{--<ul class="breadcrumb">--}}
                {{--<li><a href="/admin"><i class="icon-home2 position-left"></i> {{trans('admin.home')}}</a></li>--}}
                {{--<li><a href="/admin/users"><i class="icon-users position-left"></i> {{trans('admin.users')}}</a></li>--}}
                {{--<li class="active"><i class="icon-plus2 position-left"></i> {{ isset($user) ? trans('admin.update') : trans('admin.add') }} </li>--}}
            {{--</ul>--}}

        {{--</div>--}}
    </div>
    <!-- /page header -->

    <!-- Content area -->
    <div class="content">
    <!-- Main charts -->
        <div class="row">
            <div class="panel-body">
                <form class="form-horizontal form-validate-jquery" action="/admin/users{{ isset($user) ? '/'. $user->id : '' }}" method="post"
                      novalidate="novalidate" enctype="multipart/form-data" >
                    {!! isset($user) ? '<input type="hidden" name="_method" value="PUT">' : '' !!}
                    {{ csrf_field() }}
            <div class="col-lg-6">
                <div class="panel panel-white border-top-info">
                    <div class="panel-body">
                    <fieldset class="content-group">
                <!-- Bordered panel body table -->
                <div class="panel-heading">
                    <h6 class="panel-title">{{ isset($user) ? 'تعديل' : 'إضافة' }} عضو</h6>
                    <div class="heading-elements">
                        <div class="heading-form" >
                            <strong >التفعيل</strong>
                            <label class="switch">
                                <input type="checkbox" name="is_active" {{isset($user) ? ($user->is_active==1) ? 'checked' : '' : 'checked'}} value="{{isset($user) ? ($user->is_active==1) ? 1 : 0 : 1}}">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h5 class="panel-title">{{ isset($user) ? trans('admin.update') : trans('admin.add') }}</h5>
                        <div class="heading-elements">

                            <ul class="icons-list">

                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>

                            </ul>
                        </div>
                    </div>


                            <!-- Basic text input -->
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.name')}} <span
                                                class="text-danger">*</span></label>
                                    <div class="col-lg-9">
                                        <input type="text" name="name" class="form-control" required="required"
                                               placeholder="{{trans('admin.name')}}" aria-required="true" value="{{ isset($user) ? $user->name  : old('name') }}">
                                        @include('layouts.error', ['input' => 'name'])
                                    </div>
                                </div>
                                <!-- /basic text input -->
                    <!-- Basic text input -->
                    <div class="form-group">
                        <label class="control-label col-lg-3">{{trans('admin.user_name')}} <span
                                    class="text-danger"></span></label>
                        <div class="col-lg-9">
                            <input type="text" name="user_name" class="form-control"
                                   placeholder="{{trans('admin.user_name')}}" aria-required="true" value="{{ isset($user) ? $user->user_name  : old('user_name') }}">
                            @include('layouts.error', ['input' => 'user_name'])
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-lg-3">{{trans('admin.phone')}} <span
                                class="text-danger">*</span></label>
                        <div class="col-lg-9">
                            <input type="text" name="phone" class="form-control"  required pattern="[0-9]{11}"
                                   placeholder="{{trans('admin.phone')}}" aria-required="true" value="{{ isset($user) ? $user->phone  : old('phone') }}">
                            @include('layouts.error', ['input' => 'phone'])
                        </div>
                    </div>
                    <!-- /basic text input -->
                                <!-- Basic text input -->
                                <div class="form-group ">
                                    <label class="control-label col-lg-3">{{trans('admin.email')}} <span
                                                class="text-danger"></span></label>
                                    <div class="col-lg-9">
                                        <input type="email" name="email" class="form-control"
                                               placeholder="{{trans('admin.email')}}" aria-required="true" value="{{ isset($user) ? $user->email  : old('email') }}">
                                        @include('layouts.error', ['input' => 'email'])
                                    </div>
                                </div>
                                <!-- /basic text input -->



                                <!-- Basic text input -->
                                <div class="form-group">
                                    <label class="control-label col-lg-3">{{trans('admin.bio')}} <span
                                                class="text-danger"></span></label>
                                    <div class="col-lg-9">
                                        <input type="text" name="bio" class="form-control"
                                               placeholder="{{trans('admin.bio')}}" aria-required="true" value="{{ isset($user) ? $user->bio  : old('bio') }}">
                                        @include('layouts.error', ['input' => 'bio'])
                                    </div>
                                </div>
                                <!-- /basic text input -->




                                <!-- /basic text input -->
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label class="col-lg-3 control-label">{{trans('admin.image')}}</label>
                                    <div class="col-lg-9">
                                        <input type="file" class="file-input" data-browse-class="btn btn-primary btn-block"
                                               data-show-remove="false" data-show-caption="false" data-show-upload="false"
                                               value="" name="image">
                                        @include('layouts.error', ['input' => 'image'])
                                    </div>
                                </div>
                                <!-- End Edite -->
                            @if(isset($user))
                                <!-- /basic text input -->
                                    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                        <div class="col-lg-3"></div>
                                        <div class="col-lg-9">
                                            <img src="/images/users/{{$user->image}}" class="img-responsive center-block">
                                        </div>
                                    </div>
                            @endif
                            <!-- End Edite -->


                                <div class="form-group row">
                                    <label for="password" class="col-md-3 col-form-label text-md-right">{{trans('admin.pass')}}</label>

                                    <div class="col-md-9">
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                        @include('layouts.error', ['input' => 'password'])

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="password-confirm" class="col-md-3 col-form-label text-md-right"> {{trans('admin.confirm_pass')}}</label>

                                    <div class="col-md-9">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    </div>
                                </div>



                    </div>

</fieldset>  </div>

                    </div>
                <!-- /bordered panel body table -->
            </div>
            <div class="col-lg-6">
                <div class="panel panel-white border-top-info">
                    <div class="panel-heading">
                        <h6 class="panel-title">معلومات اضافية</h6>
                    </div>

                    <div class="panel-body">
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-3">نوع العضو</label>

                                <div class="col-lg-9">


                                            <select name="user_type" class="form-control" id="user_type" >
                                                <option value="0" {{isset($user) ? ($user->user_type==0) ? 'selected' : '' : 'selected'}}> عضو عادى</option>
                                                <option value="2" {{isset($user) ? ($user->user_type==2) ? 'selected' : '' : ''}}>إدارة</option>
                                            </select>


                                </div>

                            </div>
               </fieldset>
                        </div>
                    </div>
                <div class="panel panel-white border-top-info">
                    <div class="panel-heading">
                        <h6 class="panel-title">الأدوار و الصلاحيات</h6>
                    </div>
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <div class="form-group">
                                <label class="control-label col-lg-3">الأدوار</label>
                                <div class="col-lg-9">
                                    <select name="role" class="form-control" id="state_id" aria-required="true" >
                                        <option value="0">إختر دور</option>
                                        @foreach($roles as $role)
                                            <option value="{{ $role->name }}" @if( isset($user) && !$user->roles->isEmpty()) @if($user->getRoleNames()[0]==$role->name )selected @endif @endif>{{ $role->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="panel panel-white border-top-info">
                    <div class="panel-heading">
                        <h6 class="panel-title">{{trans('admin.wallet')}}</h6>
                    </div>
                    <div class="panel-body">
                        <fieldset class="content-group">
                            <div>
                                <label class="control-label col-lg-4">المبلغ المراد شحنه</label>
                                <div>
                                    <input type="number" id="num1" name="app_cash" class="form-control"  aria-required="true" value="{{ isset($user) ? $user->app_cash  : old('app_cash') }}">
                                    @include('layouts.error', ['input' => 'app_cash'])
                                </div>
                            </div>
                            <div>
                                <label class="control-label col-lg-3">المبلغ المستلم</label>
                                <div>
                                    <input type="number" id="num2" name="cash" class="form-control"  aria-required="true" value="{{ isset($user) ? $user->cash  : old('cash') }}">
                                    @include('layouts.error', ['input' => 'cash'])
                                </div>
                            </div>
                            @if(isset($user)&& $user->app_cash > $user->cash)
                                <div>
                                    <label class="control-label col-lg-3">المبلغ المتبقي</label>
                                    <div>
                                        <input id="remaining" class="form-control" aria-required="true" disabled>
                                    </div>
                                </div>
                                @else
                                <div>
                                    <label class="control-label col-lg-3">المبلغ المتبقي</label>
                                    <div>
                                        <input  class="form-control" aria-required="true" disabled>
                                    </div>
                                </div>
                            @endif
                        </fieldset>
                    </div>
                </div>
                </div>






            <div class="text-left">
                <button type="submit" class="btn btn-primary"><i
                            class=" icon-arrow-right7 position-left"></i> {{ isset($user) ? trans('admin.update') : trans('admin.add') }}
                </button>
            </div>
            </form>

        </div>
        <!-- /main charts -->
    </div>
</div>
    <!-- Dashboard content -->

    <!-- /dashboard content -->
@endsection
