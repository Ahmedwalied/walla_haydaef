@extends('admin.adminLayout.App')

@section('title')
    قائمة المدراء
@endsection
@section('styles')

@endsection
@section('scripts')
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/tables/datatables/extensions/select.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/plugins/forms/selects/select2.min.js')}}"></script>
    <script type="text/javascript" src="{{url('adminpanel/assets/js/pages/datatables_extension_buttons_print.js')}}"></script>
@endsection
@section('content')
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">قائمة المدراء</h5>
        </div>

        <div class="panel-body">قائمة تضم جميع المدراء</div>

        <table class="table datatable-save-state">
            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>البريد الإلكتروني</th>
                <th>دور العضو</th>
                <th>الحالة</th>
                <th class="text-center">الإجراء المتخذ</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $key=>$one)
            <tr>
                <td class="text-primary-600">{{$key+1}}</td>
                <td>{{$one->name}}</td>
                <td>{{$one->email}}</td>
                <td>@if($one->id==1)<span class="label label-info">super admin</span> @else @foreach ($one->getRoleNames() as $role) <span class="label label-info">{{$role}}</span> @endforeach @endif</td>
                <td>@if($one->is_active == 1)<span class="label label-success">مفعل</span> @else<span class="label label-danger">غير مفعل</span>@endif</td>
                <td class="text-center">
                    <ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-menu9"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                @can('users update')
                                <li><a href="{{url('admin/users/'.$one->id.'/edit ')}}" data-id="{{$one->id}}" class="text-primary-600"><i class="icon-pencil7"></i> تعديل</a></li>
                              @endcan
                                @if($one->id !=1)
                                        @can('users delete')
                                <li class="suspended-user-form">
                                    <form method="POST" action="/admin/users/{{$one->id}}" >
                                        {!!  method_field('DELETE') !!}
                                        {{ csrf_field() }}
                                        <button type="submit"  data-id="{{$one->id}}" class="text-danger-600"><i class="icon-trash"></i> حذف</button>
                                    </form>
                                </li>
                                        @endcan
                                            @can('users delete')
                                <li class="suspended-user-form">
                                    <form method="POST" action="/admin/users/{{$one->id}}" >
                                        <input type="hidden" name="suspended" value="{{($one->is_active == 1) ? 0 : 1 }}">
                                        {!!  method_field('PATCH') !!}
                                        {{ csrf_field() }}
                                    <button type="submit"  data-id="{{$one->id}}" class="text-warning-600"><i class="icon-stop"></i> {{($one->is_active == 1) ? 'إيقاف' : 'تفعيل '}}</button>
                                    </form>
                                </li>
                                            @endcan
                                @endif
                            </ul>
                        </li>
                    </ul>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @can('users create')
        <a href="/admin/users/create">
            <button type="button" name="button" style="margin: 20px;" class="btn btn-success pull-right">اضافة عضو <i class="icon-arrow-left13 position-right"></i></button>
        </a>
            @endcan
    </div>
@endsection
