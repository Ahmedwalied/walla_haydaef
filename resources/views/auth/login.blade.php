@extends(Helpers::theme_url().'.layouts.app')
@section('title')
    تسجيل الدخول
@endsection
@section('content')
    @include(Helpers::theme_url().'.includes.page-static-head',['title'=>'تسجيل الدخول'])
    <div class="cart">
        <div class="container">
            <div class="cart-detail">
                <h4 class="main-titles">تسجيل الدخول</h4>
                <div class="cart-form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input type="hidden" name="loginform" value="1">
                        <div class="form-group">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="البريد الالكتروني">
                            @if(old('loginform'))
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            @endif
                        </div>

                        <div class="form-group">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="كلمة المرور">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input class="form-check-input custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">تذكرنى</label>
                        </div>

                        <div class="form-group mb-0">
                            <div class="text-center">
                                <button type="submit" class="btn btn-login btn-block">دخول</button>

                                @if (Route::has('password.request'))
                                    <a class="forgetpass p-0" href="{{ route('password.request') }}">
                                        هل نسيت كلمة المرور ؟
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                        <p>ليس لديك حساب ؟</p>
                        <button type="button"  data-toggle="modal" data-target=".registermodel" class="btn btn-new btn-block">تسجيل جديد</button>
                </div>
            </div>
        </div>
    </div>
@endsection
