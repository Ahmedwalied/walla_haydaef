@extends(Helpers::theme_url().'.layouts.app')
@section('title')
    نسيت كلمة المرور
@endsection
@section('content')
    @include(Helpers::theme_url().'.includes.page-static-head',['title'=>'نسيت كلمة المرور'])
    <div class="about-us contact-us">
        <div class="container">
            <div class="about-bg">
                <h4 class="main-titles">نسيت كلمة المرور </h4>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">البريد الإلكتروني</label>

                            <div class="col-md-8">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group mb-0">
                            <div class="text-center">
                                <button type="submit" class="btn btn-orange">
                                    إرسال رابط إعادة تعيين كلمة المرور
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
