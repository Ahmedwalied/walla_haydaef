@extends(Helpers::theme_url().'.layouts.app')
@section('title')
    تأكيد التفعيل
@endsection
@section('content')
    @include(Helpers::theme_url().'.includes.page-static-head',['title'=>'تأكيد التفعيل'])
    <div class="about-us contact-us">
        <div class="container">
            <div class="about-bg">
                <h4 class="main-titles">تأكيد التفعيل </h4>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                </div>
            </div>
        </div>
    </div>
@endsection
