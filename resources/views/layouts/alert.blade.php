<div class="js-flash-messages  respon-sm-6">
    @if(Session::has('error'))
        <div class="alert alert-danger media">
            <button class="close media-object" data-dismiss="alert">
                <b aria-hidden="true">×</b><span class="sr-only">إخفاء</span></button>
            <div class="media-body message__body">{{ Session::get('error') }}</div>
        </div>
    @endif
    @if(Session::has('success'))
        <div class="alert alert-success media">
            <button class="close media-object" data-dismiss="alert">
                <b aria-hidden="true">×</b><span class="sr-only">إخفاء</span></button>
            <div class="media-body message__body">{{ Session::get('success') }}</div>
        </div>
    @endif

    @if(isset($errors) && $errors->any())
        <div class="alert alert-danger media">
            <button class="close media-object" data-dismiss="alert">
                <b aria-hidden="true">×</b><span class="sr-only">إخفاء</span></button>
            <div class="media-body message__body">حدثت بعض الأخطاء التالية</div>
        </div>
    @endif
</div>


