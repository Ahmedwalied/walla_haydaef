<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/





Route::group(['middleware' => 'locale', 'cors', 'namespace' => 'API'], function () {
    Route::post('register', 'UserController@register');
    Route::post('login', 'UserController@login');
//    Route::post('password_email', 'UserController@password_email');
//    Route::post('password_email_code', 'UserController@password_email_code');
//    Route::post('password_reset', 'UserController@password_reset');





});


Route::group(['middleware' => 'auth:api', 'cors','locale'], function () {
    Route::get('get_wallet', 'API\UserController@get_wallet');
    Route::post('update_wallet', 'API\UserController@update_wallet');
    Route::get('donate_records', 'API\DonateController@donate_records');
    Route::get('charity_show', 'API\DonateController@charity_show');
    Route::post('charity_donate', 'API\DonateController@charity_donate');
    Route::get('categories', 'API\CategoriesController@index');
    Route::get('profile', 'API\UserController@profile');
    Route::post('update_profile', 'API\UserController@update_profile');
    Route::post('update_image', 'API\UserController@update_image');
    Route::get('user_profile', 'API\UserController@user_profile');
    Route::get('notifications', 'API\AboutController@notification');
    Route::post('contact_us', 'API\ContactUsController@store');
    Route::post('charity_search', 'API\DonateController@charity_search');
    Route::get('terms', 'API\PageController@terms');
    Route::get('about_app', 'API\PageController@about_app');
    //    Route::put('update_bio', 'API\UserController@update_bio');
    //    Route::put('change_password', 'API\UserController@change_password');
});





