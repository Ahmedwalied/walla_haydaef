<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('lang/{lang}', ['as'=>'lang.switch', 'uses'=>'LanguageController@switchLang']);





Route::group(['middleware' => ['lang', 'IsAdmin'],'prefix' => '/admin'], function () {
    Route::get('/', 'Admin\DashboardController@index');
    Route::resource('/about', 'Admin\AboutController');
    Route::resource('/contact', 'Admin\ContactController');
    Route::resource('/info', 'Admin\SiteInformationController');
    Route::resource('/pages', 'Admin\PageController');
//    Route::resource('/categories', 'Admin\CategoriesController');
    Route::resource('/foundations', 'Admin\FoundationController');
    Route::resource('/branches', 'Admin\BranchController');
    Route::resource('/charities', 'Admin\CharityController');
    Route::resource('/clients', 'Admin\ClientController');
    Route::resource('/contact-us', 'Admin\ContactUsController');
    Route::resource('/users', 'Admin\UsersController');
    Route::resource('/funds', 'Admin\FundController');
    Route::resource('/transfers', 'Admin\TransferController');
    Route::get('/users-roles', 'Admin\UsersController@usersRoles');
    Route::resource('/roles', 'Admin\RolesController');
    Route::resource('/admins', 'Admin\AdminsController');
    Route::get('/notifications', 'Admin\NotificationsController@index');
    Route::get('/settings', 'Admin\SettingController@index');
    Route::post('/settings/store', 'Admin\SettingController@store');
    Route::get('active/{id}' , 'Admin\FoundationController@active');
    Route::get('disActive/{id}' , 'Admin\FoundationController@disActive');
//    Route::get('/users/{id}/block', 'UsersController@block');
//    Route::get('/users/{id}/disblock', 'UsersController@disblock');

});



Route::get('/admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Admin\LoginController@login')->name('admin.login');
Route::get('/home', 'HomeController@index')->name('home');



Route::group(['middleware' => ['lang', 'web']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/country/{country}/cities', 'CountriesController@getCities');
    Route::resource('/about', 'AboutController');


});

Route::get('/cache', function () {
    $exitCode = \Illuminate\Support\Facades\Artisan::call('cache:clear');
    return 'cache';

});

Auth::routes();
Route::any('{query}',
    function() { return redirect('/admin/login'); })
    ->where('query', '.*');

//Route::get('/', 'HomeController@index')->name('home');
